-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 24, 2024 at 07:18 AM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `herbnangel`
--

-- --------------------------------------------------------

--
-- Table structure for table `branch_record`
--

CREATE TABLE `branch_record` (
  `code` varchar(11) NOT NULL,
  `branch_name` varchar(100) NOT NULL,
  `branch_address` varchar(255) NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `contact_number` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `branch_record`
--

INSERT INTO `branch_record` (`code`, `branch_name`, `branch_address`, `contact_person`, `contact_number`, `email`) VALUES
('K1', 'Branch 1', 'Katapatan', 'Herb', '09123456789', 'test@gmail.com'),
('K2', 'Branch 2', 'Cabuyao', 'secret', '09123456789', 'test1@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `dealer_record`
--

CREATE TABLE `dealer_record` (
  `code` varchar(11) NOT NULL,
  `dealer_name` varchar(100) NOT NULL,
  `dealer_address` varchar(255) NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `contact_number` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `dealer_record`
--

INSERT INTO `dealer_record` (`code`, `dealer_name`, `dealer_address`, `contact_person`, `contact_number`, `email`) VALUES
('13634', 'Dealer 1', 'Blk 14 Lot 14', 'Sheena Jane Bacar Toroy', '09090852551', 'toroysheena@gmail.com'),
('TEST', 'TEST', 'TEST', 'TEST', 'TEST', 'TEST');

-- --------------------------------------------------------

--
-- Table structure for table `inflow_admin`
--

CREATE TABLE `inflow_admin` (
  `date` datetime NOT NULL,
  `barcode` int(11) NOT NULL,
  `supplier_price` float NOT NULL,
  `units_received` int(11) NOT NULL,
  `totalvalue` varchar(100) NOT NULL,
  `code` int(11) NOT NULL,
  `location` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `inflow_admin`
--

INSERT INTO `inflow_admin` (`date`, `barcode`, `supplier_price`, `units_received`, `totalvalue`, `code`, `location`) VALUES
('2024-03-12 21:46:41', 87, 10, 10, '100', 1, 'Admin'),
('2024-03-12 22:41:12', 87, 10, 10, '100', 1, 'Admin'),
('2024-03-12 22:42:11', 3, 5, 10, '50', 1, 'Admin'),
('2024-03-12 22:42:29', 78, 10, 10, '100', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `inflow_branch`
--

CREATE TABLE `inflow_branch` (
  `date` datetime NOT NULL,
  `barcode` int(11) NOT NULL,
  `supplier_price` float NOT NULL,
  `units_received` int(11) NOT NULL,
  `totalvalue` varchar(100) NOT NULL,
  `code` varchar(199) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `inflow_branch`
--

INSERT INTO `inflow_branch` (`date`, `barcode`, `supplier_price`, `units_received`, `totalvalue`, `code`) VALUES
('2024-03-10 16:25:52', 10, 10, 10, '100', 'K1'),
('2024-03-10 23:01:28', 99, 5, 10, '50', 'K2');

-- --------------------------------------------------------

--
-- Table structure for table `mechanic_record`
--

CREATE TABLE `mechanic_record` (
  `mechanic_id` int(11) NOT NULL,
  `mechanic_name` varchar(100) NOT NULL,
  `user_branch_code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `mechanic_record`
--

INSERT INTO `mechanic_record` (`mechanic_id`, `mechanic_name`, `user_branch_code`) VALUES
(1, 'Juan Tamad', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mechanic_services`
--

CREATE TABLE `mechanic_services` (
  `date` datetime NOT NULL,
  `mechanic_service_id` int(11) NOT NULL,
  `mechanic_id` int(11) NOT NULL,
  `services_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `mechanic_services`
--

INSERT INTO `mechanic_services` (`date`, `mechanic_service_id`, `mechanic_id`, `services_id`, `price`, `code`) VALUES
('2024-03-14 12:12:00', 1, 1, 3, 50, 1),
('2024-03-14 12:12:00', 2, 1, 4, 50, 1),
('2024-03-14 12:12:00', 3, 1, 5, 50, 1),
('2024-03-14 12:12:00', 4, 1, 9, 50, 1),
('2024-03-13 15:02:15', 5, 1, 9, 10, 1),
('2024-03-13 15:03:55', 6, 1, 12, 800, 1);

-- --------------------------------------------------------

--
-- Table structure for table `outflow_selling`
--

CREATE TABLE `outflow_selling` (
  `date` datetime NOT NULL,
  `barcode` int(11) NOT NULL,
  `selling_price` float NOT NULL,
  `units_sold` int(11) NOT NULL,
  `total_value_selling` int(11) NOT NULL,
  `total_value_supplier` int(11) NOT NULL,
  `profit` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `total_profit` int(11) NOT NULL,
  `code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `outflow_selling`
--

INSERT INTO `outflow_selling` (`date`, `barcode`, `selling_price`, `units_sold`, `total_value_selling`, `total_value_supplier`, `profit`, `discount`, `total_profit`, `code`) VALUES
('2024-03-11 20:56:14', 10, 15, 1, 15, 10, 5, 0, 5, 1),
('2024-03-11 20:57:45', 10, 16, 1, 16, 10, 6, 0, 6, 1),
('2024-03-11 22:35:13', 10, 15, 1, 15, 10, 5, 0, 5, 1),
('2024-03-12 14:23:52', 10, 20, 1, 20, 10, 10, 0, 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `outflow_selling_branch`
--

CREATE TABLE `outflow_selling_branch` (
  `date` datetime NOT NULL,
  `barcode` int(11) NOT NULL,
  `selling_price` float NOT NULL,
  `units_sold` int(11) NOT NULL,
  `total_value_selling` int(11) NOT NULL,
  `total_value_supplier` int(11) NOT NULL,
  `profit` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `total_profit` int(11) NOT NULL,
  `code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `outflow_selling_branch`
--

INSERT INTO `outflow_selling_branch` (`date`, `barcode`, `selling_price`, `units_sold`, `total_value_selling`, `total_value_supplier`, `profit`, `discount`, `total_profit`, `code`) VALUES
('2024-03-11 21:16:36', 10, 15, 1, 15, 10, 5, 0, 5, 1),
('2024-03-12 14:24:38', 10, 20, 1, 20, 10, 10, 0, 10, 1),
('2024-03-14 14:25:06', 10, 15, 1, 15, 10, 5, 0, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `barcode` int(11) NOT NULL,
  `productname` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`barcode`, `productname`, `description`, `model`) VALUES
(1, '44D BELT', '', ''),
(2, ' 2PH BELT', '', ''),
(3, 'KVY BELT', '', ''),
(4, 'CLICK BELT', '', ''),
(5, 'K44 BELT BEAT CARB', '', ''),
(6, '2DP BELT', '', ''),
(7, '5TL BELT', '', ''),
(8, 'B65 BELT', '', ''),
(9, '2PV HEAD GASKET ', '', ''),
(10, 'MX HEAD GASKET ', '', ''),
(11, '2DP HEAD GASKET', '', ''),
(12, 'INTAKE VALVE', '', ''),
(13, 'EXHAUST VALVE ', '', ''),
(14, 'IDLE GEAR ORIG.', '', ''),
(15, '17812 OIL SEAL', '', ''),
(16, '19814 OIL SEAL', '', ''),
(17, '19800 OIL SEAL', '', ''),
(18, 'CHAIN GUIDE UP', '', ''),
(19, 'CHAIN GUIDE DOWN', '', ''),
(20, 'ROCKER ARM MIO', '', ''),
(21, 'SLIDER PIECE HONDA', '', ''),
(22, 'SLIDER PIECE YAMAHA', '', ''),
(23, 'TD NUT 5TL', '', ''),
(24, 'TD NUT 2DP', '', ''),
(25, 'VALVE SEAL HONDA', '', ''),
(26, 'YAMAHA VALVE SEAL', '', ''),
(27, 'RAIDER 150 OIL FILTER', '', ''),
(28, 'BETAGREY ', '', ''),
(29, 'HORN RELAY', '', ''),
(30, 'DOMINO SWITCH MDL left and right', '', ''),
(31, 'SPORTY CENTER SPRING 1000 RPM', '', ''),
(32, 'SPORTY CENTER SPRING 1500 RPM', '', ''),
(33, 'MIO I 125 CENTER SPRING 1500 RPM', '', ''),
(34, 'NMAX 155 CENTER SPRING 1500 RPM', '', ''),
(35, 'JETTINGS 130', '', ''),
(36, 'DRIVE FACE MIO MTRT', '', ''),
(37, 'BELL CLICK MTRT', '', ''),
(38, 'BELL AEROX/NMAX MTRT', '', ''),
(39, 'CONNECTING ROD MIO 1.5 MTRT', '', ''),
(40, 'RACING FAN MIO MTRT', '', ''),
(41, 'FLY BALL 6G. MIO MTRTX', '', ''),
(42, 'FLY BALL 8G. MIO MTRT', '', ''),
(43, 'NUT 19 MM', '', ''),
(44, 'M8 ', '', ''),
(45, 'M10', '', ''),
(46, 'M12', '', ''),
(47, 'M14', '', ''),
(48, 'M17', '', ''),
(49, 'FLY BALL13G CLICK 125 MTRT', '', ''),
(50, 'SUPER SPEED SHOCK MIO', '', ''),
(51, 'SUPER DRAG RIM', '', ''),
(52, 'STRONG RIM ', '', ''),
(53, 'YAMALUBE GEAR OIL', '', ''),
(54, 'MOTUL OIL 0.8L ', '', ''),
(55, 'MOTUL OIL 1L', '', ''),
(56, 'PIAA HORN', '', ''),
(57, 'MDL MINI DRIVING LIGHT MOKOTO', '', ''),
(58, 'FORK OIL PETRON ', '', ''),
(59, 'TIRE SEALANT KOBY', '', ''),
(60, 'SUPER SPEED OIL 0.8 SCOOTER', '', ''),
(61, 'SUPER SPEED OIL 1L SCOOTER', '', ''),
(62, 'SUPER SPEED OIL 1L UNDERBONE ', '', ''),
(63, 'SUPER SPEED OIL 0.8 UNDERBONE', '', ''),
(64, 'RS8 ECO', '', ''),
(65, 'ELECTRICAL TAPE', '', ''),
(66, 'GRASA KOBY', '', ''),
(67, 'GRASA TOP1 FORMULA', '', ''),
(68, 'BULB', '', ''),
(69, 'SPARK PLUG 2120 D8EA', '', ''),
(70, 'SPARK PLUG 6899 CPR6EA-9 ', '', ''),
(71, 'SPARK PLUG 4629 C7HSA', '', ''),
(72, 'MTRT CAMSHAFT MB710 MIO 125 ST 2', '', ''),
(73, 'CAMSHAFT MB555 NMAX ST 2', '', ''),
(74, 'MTRT CAMSHAFT MB177 RACING MIO ST 2', '', ''),
(75, 'MTRT CAMSHAFT MB178 RACING MIO ST 3', '', ''),
(76, 'MTRT CAMSHAFT MB535 MIO 125 ST 1', '', ''),
(77, 'MTRT VALVE SHIM MB0390 4.5MM', '', ''),
(78, 'MTRT ROCKER ARM MIO MB131 MTRT', '', ''),
(79, 'LIGHTEN VALVES MIO MB0337 1A 20/23MM 5MM', '', ''),
(80, 'RCM GASKET ALLOY 1MM MIO', '', ''),
(81, 'RCM GASKET ALLOY 2MM MIO', '', ''),
(82, 'RCM LIGHTEN GEAR 15/39', '', ''),
(83, 'RCM ROCKER ARM RACING MONKEY MIO', '', ''),
(84, 'RCM LIGHTEN VALVES 19/22 R150 STANDARD', '', ''),
(85, 'RCM LIGHTEN VALVES 20/23 MIO', '', ''),
(86, 'RCM CLUTCH LINING ASSY. LIGHTEN COPPER MIO', '', ''),
(87, 'RCM IDLE GEAR MIO RACING MONKEY ', '', ''),
(88, 'RCM BUNG CENSOR RACING MONKEY', '', ''),
(89, 'RCM RACING OIL PUMP GEAR MIO', '', ''),
(90, 'RCM CRANK SHAFT BEARING 63/22.15*6305 MIO', '', ''),
(91, 'RCM T POST BALL RACE BLUE V2 AEROX', '', ''),
(92, 'RCM T POST BALL RACE LC-150', '', ''),
(93, 'RCM T POST BALL RACE RAIDER 150', '', ''),
(94, 'CLUTCH SPRING MB 0304 1000 RPM MIO MTRT', '', ''),
(95, 'CLUTCH SPRING MB 0305 1500 RPM MIO MTRT', '', ''),
(96, 'VALVE RETAINER MTRT', '', ''),
(97, 'RCM PIN DOWEL ', '', ''),
(98, 'DAYTONA GRIP', '', ''),
(99, 'DOMINO GRIP RED', '', ''),
(100, 'DOMINO GRIP GRAY', '', ''),
(101, 'ARIETE GRIP', '', ''),
(102, 'MSD IGNITION COIL SET', '', ''),
(103, 'CRANK CASE CNC RAIDER FI ', '', ''),
(104, 'CRANK CASE CNC RAIDER CARB', '', ''),
(105, 'BODY YAMAHA BODY BOLTS CNC 5X15', '', ''),
(106, 'CRANK CASE CNC WAVE 125 R/S', '', ''),
(107, 'CRANKCASE HENG CNC MIO 125', '', ''),
(108, 'HENG CNC M6 BOLTS ', '', ''),
(109, 'CNC HEAD NUT', '', ''),
(110, 'RCM ROCKER ARM NMAX', '', ''),
(111, 'RCM HI NUT 8MM UNIVERSAL ', '', ''),
(112, 'RCM CRANK SHAFT OIL SEAL MIO', '', ''),
(113, 'RCM ROCKER ARM WAVE 125', '', ''),
(114, 'FORMULA 1 CALIPER RED/BLACK', '', ''),
(115, 'PS16', '', ''),
(116, 'RCS BREMBO DUAL TANK', '', ''),
(117, 'NGO CALIPER MIO', '', ''),
(118, 'DORAEMON CALIPER W/ BRACKET W125', '', ''),
(119, 'DOMINO LEVER', '', ''),
(120, '62MM BLOCK CB NMAX 155 MTRT', '', ''),
(121, '59MM BLOCK SPORTY METAL MTRT ', '', ''),
(122, '63MM BLOCK CB SPORTY MTRT', '', ''),
(123, 'KEIHIN CARB 24MM', '', ''),
(124, 'KEIHIN CARB 26MM ', '', ''),
(125, 'KEIHIN CARB BARAKO', '', ''),
(126, 'RED SPEED DEGREASER ', '', ''),
(127, 'B65 LINING ', '', ''),
(128, '2PH CONNECTING ROD M3 ', '', ''),
(129, '44D DRIVE FACE', '', ''),
(130, 'OIL FILTER YAMAHA ', '', ''),
(131, '2DP DRIVE FACE ', '', ''),
(132, '5TL DRIVE FACE', '', ''),
(133, '5TL PULLY', '', ''),
(134, '2SX PULLY ', '', ''),
(135, 'CLICK 125 LINING HONDA', '', ''),
(136, 'YAMAHA CENTER SPRING MIO SPORTY', '', ''),
(137, 'MTRT PRIMARY GEAR 16T', '', ''),
(138, 'RCM CLUTCH SPRING MIO I 125 1200RPM', '', ''),
(139, 'RCM CLUTCH SPRING  SPORTY 1000RPM', '', ''),
(140, '24*28 BIG VALVES HEAD WAVE 125', '', ''),
(141, '24*28 BIG VALVES MIO', '', ''),
(142, 'CNC BLOCK 54 SPORTY', '', ''),
(143, 'CNC BLOCK STANDARD SPORTY', '', ''),
(144, 'CNC BLOCK 59 SPORTY', '', ''),
(145, 'CNC BLOCK STANDARD MIO I 125 ', '', ''),
(146, 'CNC BLOCK XRM 110 53', '', ''),
(147, 'CNC BLOCK WAVE 125 57', '', ''),
(148, 'MTK XRM 53 BLOCK', '', ''),
(149, '5TL HEAD MIO STANDARD ', '', ''),
(150, 'CNC 500 CC STARTER SPORTY', '', ''),
(151, 'RS8 R9 OIL 1L ', '', ''),
(152, 'YAMALUBE SCOOTER 0.8L ', '', ''),
(153, 'YAMALUBE SCOOTER 1L', '', ''),
(154, 'VS1 ', '', ''),
(155, 'POWER COOLANT TOP1', '', ''),
(156, 'BARAKO OIL FILTER ', '', ''),
(157, 'BALL RACE YAMAHA', '', ''),
(158, 'BALL RACE WAVE ', '', ''),
(159, 'STARTER RELAY M3', '', ''),
(160, 'YAGUSO RIOS 120', '', ''),
(161, 'YAGUSO RIOS 155', '', ''),
(162, 'YAGUSO RIOS 157', '', ''),
(163, 'RCM CLUTCH SPRING SPORTY 1200RPM', '', ''),
(164, 'YAGUSO RIOS 185', '', ''),
(165, 'YAGUSO RIOS 150', '', ''),
(166, 'L9 M3 CAMSHAFT STANDARD', '', ''),
(167, 'THROTTLE BODY CLEANER BG.', '', ''),
(168, '44K BG.', '', ''),
(169, 'FUSE 15 AMP. ', '', ''),
(170, 'FUSE 10 AMP.', '', ''),
(171, 'BODY SCREW WITH CLIP', '', ''),
(172, 'DOMINO SWITCH LEFT', '', ''),
(173, 'MOKOTO LED HEAD LIGHT TRILED', '', ''),
(174, 'RZ 6305 2RS', '', ''),
(175, 'KOYO 6304 2RS', '', ''),
(176, 'CSL 6304', '', ''),
(177, 'KOYO 6303', '', ''),
(178, 'AST 6303', '', ''),
(179, 'KOYO 6302', '', ''),
(180, 'KOYO 6301', '', ''),
(181, 'RZ 6301', '', ''),
(182, 'RZ 6205', '', ''),
(183, 'KOYO 6205', '', ''),
(184, 'KOYO 6204', '', ''),
(185, 'AST 6204', '', ''),
(186, 'KOYO 6203', '', ''),
(187, 'RZ 6203', '', ''),
(188, 'KOYO 6202', '', ''),
(189, 'RZ 6202', '', ''),
(190, 'KOYO 6201', '', ''),
(191, 'RZ 6200', '', ''),
(192, 'KOYO 6005', '', ''),
(193, 'CSL 6005', '', ''),
(194, 'KOYO 6004', '', ''),
(195, 'RZ 6004', '', ''),
(196, 'KOYO 6003', '', ''),
(197, 'NTN 6200', '', ''),
(198, 'KOYO 6200', '', ''),
(199, 'KOYO 6001', '', ''),
(200, 'KOYO 638', '', ''),
(201, 'KOYO 628', '', ''),
(202, 'YB6.5L-BS XTREME BATTERY', '', ''),
(203, '12N7BL-BS XTREME BATTERY', '', ''),
(204, 'YB3L-BS XTREME BATTERY', '', ''),
(205, 'YB5L-BS XTREME BATTERY', '', ''),
(206, 'YTX4L-BS XTREME BATTERY', '', ''),
(207, 'YTX7L-BS XTREME BATTERY', '', ''),
(208, 'STATOR WAVE 125', '', ''),
(209, 'KITACO RIGHT HANDLE SWITCH', '', ''),
(210, 'MICHELIN 70*90-14', '', ''),
(211, 'MICHELIN 80*80-14', '', ''),
(212, 'BRAKE MASTER SET MIO', '', ''),
(213, 'BRAKE MASTER SET WAVE 125', '', ''),
(214, 'JRP SEAT COVER NMAX ', '', ''),
(215, 'JRP SEAT COVER AEROX', '', ''),
(216, 'JRP SEAT COVER MEDIUM', '', ''),
(217, 'JRP SEAT COVER SMALL', '', ''),
(218, 'FUEL HOSE 10METERS BLACK', '', ''),
(219, 'FUEL HOSE 10METERS WHITE', '', ''),
(220, 'SURE BRAKE FLUID', '', ''),
(221, 'DOMINO HANDLE SWITCH SET MIO STOCK', '', ''),
(222, 'SPARKPLUG CAP L TYPE', '', ''),
(223, '3 HOLES AMORE FAIRINGS ', '', ''),
(224, 'BUTA DISC. WAVE 125', '', ''),
(225, 'APR FLAT SEAT NMAX V1', '', ''),
(226, 'APR FLATSEAT WAVE 125 I', '', ''),
(227, 'ORDINARY SEAT COVER', '', ''),
(228, 'BRAKE PAD WAVE 110', '', ''),
(229, 'BRAKE PAD CB110', '', ''),
(230, 'BRAKE PAD ADV 150 REAR', '', ''),
(231, 'BRAKE PAD RS 100', '', ''),
(232, 'BRAKE PAD ADDRESS', '', ''),
(233, 'BRAKE PAD BEAT ', '', ''),
(234, 'BRAKE PAD WIND 125', '', ''),
(235, 'BRAKE PAD SHOGUN ', '', ''),
(236, 'BRAKE PAD RAIDER 150 FI', '', ''),
(237, 'BRAKE PAD RAIDER 150 ', '', ''),
(238, 'BRAKE PAD CBR 150', '', ''),
(239, 'BRAKE PAD WAVE 125 ', '', ''),
(240, 'BRAKE PAD DASH', '', ''),
(241, 'BRAKE PAD SNIPER 135', '', ''),
(242, 'BRAKE PAD KLX 150', '', ''),
(243, 'BRAKE PAD PCX 150', '', ''),
(244, 'ADJUSTABLE TENSIONER R150', '', ''),
(245, 'ADJUSTABLE TENSIONER MIO', '', ''),
(246, 'ADJUSTABLE TENSIONER W125', '', ''),
(247, 'DUST SEAL HONDA SET', '', ''),
(248, 'FRONT FORK OILSEAL', '', ''),
(249, 'REPAIR KIT YAMAHA ', '', ''),
(250, 'REPAIR KIT HONDA', '', ''),
(251, 'AIR FILTER CLICK', '', ''),
(252, 'AIR FILTER SNIPER 135', '', ''),
(253, 'AIR FILTER SNIPER 150', '', ''),
(254, 'AIR FILTER MIO SOUL', '', ''),
(255, 'AIR FILTER SPORTY', '', ''),
(256, 'AIR FILTER NMAX', '', ''),
(257, 'AIR FILTER SHOGUN ', '', ''),
(258, 'AIR FILTER PCX ', '', ''),
(259, 'AIR FILTER DASH 110', '', ''),
(260, 'AIR FILTER MIO I 125', '', ''),
(261, 'AIR FILTER SUPER VALIANT M3', '', ''),
(262, 'RUDDER TIRE 45*90-17', '', ''),
(263, 'RUDDER TIRE 60*80-17', '', ''),
(264, 'RUDDER TIRE 70*80-17', '', ''),
(265, 'RUDDER TIRE 80*80-17', '', ''),
(266, 'BREMBO 4POT', '', ''),
(267, 'BREMBO 2POT', '', ''),
(268, 'LIGHTEN DISC MIO I 125', '', ''),
(269, 'GY6 125 VALVE', '', ''),
(270, 'BEAT VALVE ', '', ''),
(271, 'SMASH VALVE  ', '', ''),
(272, 'SPORTY VALVE', '', ''),
(273, 'TTGR FLY BALL NMAX 8G.', '', ''),
(274, 'TTGR FLY BALL NMAX 9G.', '', ''),
(275, 'TTGR FLY BALL NMAX 10G.', '', ''),
(276, 'TTGR FLY BALL NMAX 11G.', '', ''),
(277, 'TTGR FLY BALL MIO SPORTY 7G.', '', ''),
(278, 'TTGR FLY BALL MIO SPORTY 9G.', '', ''),
(279, 'RCS BRAKE MASTER', '', ''),
(280, 'LIGHTEN FRONT SHOCK w125', '', ''),
(281, 'TURTLE BACK CALIPER WAVE 125', '', ''),
(282, 'MINI GP CALIPER', '', ''),
(283, 'BRAKE MASTER XRM 110', '', ''),
(284, 'BRAKE MASTER WAVE 125', '', ''),
(285, 'BRAKE MASTER MIO', '', ''),
(286, 'FLASHER RELAY ', '', ''),
(287, 'HORN PIPPIP', '', ''),
(288, '4 PIN CDI', '', ''),
(289, 'ROTATOR ', '', ''),
(290, 'SPEEDOMETER GEAR BOX MIO', '', ''),
(291, 'FUSE BOX', '', ''),
(292, 'SPARKPLUG CAP MIO', '', ''),
(293, 'FUEL FILTER ', '', ''),
(294, 'THROTTLE CABLE XRM LONG ', '', ''),
(295, 'THROTTLE CABLE SHOGUN ', '', ''),
(296, 'THROTTLE RAIDER J 110', '', ''),
(297, 'THROTTLE CABLE XRM 125', '', ''),
(298, 'THROTTLE RS 100', '', ''),
(299, 'THROTTLE CRYPTON', '', ''),
(300, 'THROTTLE BARAKO', '', ''),
(301, 'THROTTLE STX', '', ''),
(302, 'THROTTLE WAVE 100', '', ''),
(303, 'THROTTLE RAIDER 150', '', ''),
(304, 'THROTTLE DREAM 100', '', ''),
(305, 'THROTTLE CABLE MIO SPORTY ', '', ''),
(306, 'RCM CLUTCH SPRING SPORTY 1500RPM', '', ''),
(307, 'RCM CENTER SPRING 1000RPM SPORTY ', '', ''),
(308, 'HEADLIGHT OSRAM YELLOW', '', ''),
(309, 'SINGLE WIRE', '', ''),
(310, 'QR CODE BREMBO', '', ''),
(311, 'RCM CENTER SPRING 1200RPM SPORTY ', '', ''),
(312, 'LHK CAMSHAFT STAGE 1 2 3', '', ''),
(313, 'RCM CENTER SPRING 1500RPM SPORTY', '', ''),
(314, 'APR SEAT COVER RED', '', ''),
(315, 'APR SEAT COVER BLUE ', '', ''),
(316, 'APR SEAT COVER BLACK ', '', ''),
(317, 'KITTI CLUTCH SPRING WAVE 110', '', ''),
(318, 'KEIHIN 28MM CARBURATOR', '', ''),
(319, 'RCM CENTER SPRING 1000RPM NMAX/AEROX', '', ''),
(320, 'VALVES SMASH 115', '', ''),
(321, 'VALVES MIO SOUL I 115 ', '', ''),
(322, 'VALVES SMASH ', '', ''),
(323, 'VALVES BEAT ', '', ''),
(324, 'VALVES GY6 125', '', ''),
(325, 'MIO I 125 / NMAX TTGR FLYBALL 8G', '', ''),
(326, 'MIO I 125 / NMAX TTGR FLYBALL 9G', '', ''),
(327, 'MIO I 125 / NMAX TTGR FLYBALL 10G', '', ''),
(328, 'MIO I 125 / NMAX TTGR FLYBALL 11G', '', ''),
(329, 'MIO SPORTY TTGR FLYBALL  9G', '', ''),
(330, 'CNC MIO I 125 59 BLOCK', '', ''),
(331, 'BREMBO CALIPER AEROX 155', '', ''),
(332, 'FUEL FILTER YAMAHA', '', ''),
(333, 'FUEL FILTER HONDA ', '', ''),
(334, 'RCM CENTER SPRING 1200RPM NMAX/AEROX', '', ''),
(335, 'RCM CENTER SPRING 1500RPM NMAX/AEROX', '', ''),
(336, 'BRACKET CALIPER 2 POT ', '', ''),
(337, 'BRACKET CALIPER 4 POT ', '', ''),
(338, 'EARLS HOSE REAR', '', ''),
(339, 'RCM INJECTOR MIO I 125 130CC', '', ''),
(340, 'HUB AND MILE', '', ''),
(341, 'P16 BRAKE MASTER', '', ''),
(342, 'OEM THROTTLE YAMAHA', '', ''),
(343, 'CNC WAVE 100 BLOCK 54', '', ''),
(344, 'RCM CLUTCH LINING PAD ONLY', '', ''),
(345, 'BREMBO MOUSE CALIPER SNIPER BLACK', '', ''),
(346, 'SUPER SPEED ENGINE SUPPORT MIO +2.5', '', ''),
(347, 'ENGINE VALVES STOCK  W125 L9 ', '', ''),
(348, 'ENGINE VALVES STOCK  TMX  CDI L9', '', ''),
(349, 'ENGINE VALVES STOCK RUSI 150 L9', '', ''),
(350, 'ENGINE VALVES STOCK TMX SUPREMO L9', '', ''),
(351, 'DRAIN PLUG SPORTY L9', '', ''),
(352, 'IDLE GEAR SPORTY L9', '', ''),
(353, 'JRP FLAT SEAT OREO NMAX V2', '', ''),
(354, 'CNC AXLE RAIDER 150 FRONT/REAR', '', ''),
(355, 'CNC AXLE WAVE BACK', '', ''),
(356, 'CNC AXLE MIO', '', ''),
(357, 'CNC AXLE NMAX', '', ''),
(358, 'CNC AXLE AEROX', '', ''),
(359, 'CNC AXLE CLICK', '', ''),
(360, 'CNC FLUID BOLTS', '', ''),
(361, 'VENTILATED DISC ', '', ''),
(362, 'CNC OIL BOLTS YAMAHA', '', ''),
(363, 'CNC GEAR OIL BOLTS', '', ''),
(364, 'CNC BODY SCREW FLAT ', '', ''),
(365, 'CNC AXLE PCX ', '', ''),
(366, 'CNC BANJO BOLTS', '', ''),
(367, 'THAI FLOWER ', '', ''),
(368, 'CAMSHAFT BEAT FI L9', '', ''),
(369, 'CAMSHAFT MIO I125 L9', '', ''),
(370, 'CAMSHAFT NMAX  L9', '', ''),
(371, 'ENGINE VALVES CT100 L9', '', ''),
(372, 'ENGINE VALVES XRM 110 L9 ', '', ''),
(373, 'ENGINE VALVES SPORTY L9', '', ''),
(374, 'MSD CABLE ', '', ''),
(375, 'BREMBO CALIPER MINI GP 4POT', '', ''),
(376, 'SUN BELL MIO I125', '', ''),
(377, 'SUPER SPEED SHOCK WAVE ', '', ''),
(378, 'UNIVERSAL THROTTLE CABLE ', '', ''),
(379, 'RS8 CVT CLEANER', '', ''),
(380, 'WAVE 125 LIGHTEN DISC', '', ''),
(381, 'LIGHTEN DISC MIO', '', ''),
(382, 'FRONT SHOCK SET STD MIO', '', ''),
(383, 'PULLEY SET BEAT FI M POWER', '', ''),
(384, 'SPARKPLUG CAP RAIDER', '', ''),
(385, 'PULLEY SET CLICK', '', ''),
(386, 'PULLEY SET BEAT CARB M POWER', '', ''),
(387, 'RAM AIR PCX ', '', ''),
(388, 'RAM AIR AEROX', '', ''),
(389, 'RAM AIR NMAX', '', ''),
(390, 'RCM SPORTY FLYBALL 6G', '', ''),
(391, 'RCM SPORTY FLYBALL 7G', '', ''),
(392, 'RCM FLYBALL SPORTY 9G', '', ''),
(393, 'RCM FLYBALL MIO I 125 8G', '', ''),
(394, 'RCM FLYBALL MIO I 125 9G', '', ''),
(395, 'RCM FLYBALL CLICK 125 10G', '', ''),
(396, 'PS16 REPAIR KIT', '', ''),
(397, 'CONCAVE WHITE BOLTS', '', ''),
(398, 'RCM FLYBALL CLICK 125 11G ', '', ''),
(399, 'RCM FLYBALL BEAT FI 8G', '', ''),
(400, 'RCM FLYBALL BEAT FI 9G', '', ''),
(401, 'RCM FLYBALL BEAT FI 10G', '', ''),
(402, 'RCM FLYBALL BEAT FI 11G', '', ''),
(403, 'MTRT FLYBALL 8G MIO I 125X', '', ''),
(404, 'MTRT FLYBALL 11G MIO I 125', '', ''),
(405, 'MTRT FLYBALL 13G MIO I 125X', '', ''),
(406, 'MTRT FLYBALL 14G MIO I 125X', '', ''),
(407, 'HYLOS H530 SNIPER 17 (RED/BLACK)', '', ''),
(408, 'HYLOS H530 CLICK 17 (BLUE)', '', ''),
(409, 'SPEEDOMETER CABLE NOUVO MIO3', '', ''),
(410, '81141', '', ''),
(411, '81131', '', ''),
(412, '64308', '', ''),
(413, '80151', '', ''),
(414, 'KOBY STICKER REMOVER', '', ''),
(415, 'F7481-00', '', ''),
(416, '400-N650-01', '', ''),
(417, 'VANZ BOY (SILVER)', '', ''),
(418, 'PULLEY SET RS8 NMAX/AEROX ', '', ''),
(419, 'MTV RAIDER FI 1650', '', ''),
(420, 'MTV MIO FLAT SEAT ', '', ''),
(421, 'CNC DISC BOLTS ', '', ''),
(422, 'APR FLAT SEAT RAIDER CARB BLACK carbon', '', ''),
(423, 'BRAKE MASTER RCS CORSA 10A COPY', '', ''),
(424, 'STARTER RELAY XRM', '', ''),
(425, 'IGNITION COIL', '', ''),
(426, 'MAGIC GATAS', '', ''),
(427, 'SWITS HOSE ', '', ''),
(428, 'TMX 155 CARBURATOR', '', ''),
(429, 'PETRON 4T 200ML', '', ''),
(430, 'PULLEY SET SPORTY RS8', '', ''),
(431, 'BELL CLICK RS8 ', '', ''),
(432, 'BELL MIO I RS8', '', ''),
(433, 'BELL SPORTY RS8', '', ''),
(434, 'BELL NMAX RS8', '', ''),
(435, '50/100 EMD', '', ''),
(436, 'PODIUM WAVE 125', '', ''),
(437, 'PODIUM WAVE 100', '', ''),
(438, 'PODIUM NMAX V2', '', ''),
(439, 'DOMINO QUICK THROTTLE (UNIVERSAL)', '', ''),
(440, 'BRAKE FLUID TANK CAP BRAKE MASTER FLUID', '', ''),
(441, 'AEROX/NMAX MINI DRIVING LIGHT BRACKET ', '', ''),
(442, 'HONDA CLICK LED LIGHT BRACKET FOR MDL     ', '', ''),
(443, 'DOMINO HANDLE SWITCH FOR HONDA CLICK', '', ''),
(444, 'BRAKE CABLE MIO SPORTY', '', ''),
(445, 'APIDO CDI SPORTY', '', ''),
(446, 'PDD S25 MOTORCYCLE TAIL LIGHT', '', ''),
(447, ' MTR HANDLE SWITCH MIO STOCK', '', ''),
(448, 'PDD YAMAKOTO STAINLESS STEEL SPOKE 10*155', '', ''),
(449, 'PDD YAMAKOTO STAINLESS STEEL SPOKE 10*157', '', ''),
(450, 'PDD YAMAKOTO STAINLESS STEEL SPOKE 10*150', '', ''),
(451, 'PDD TIRE VALVE, PITO TUBELESS', '', ''),
(452, 'PDD YAMAKOTO STAINLESS STEEL SPOKE 10*129', '', ''),
(453, 'PDD YAMAKOTO STAINLESS STEEL SPOKE 10*161', '', ''),
(454, 'PDD YAMAKOTO STAINLESS STEEL SPOKE 10*120', '', ''),
(455, 'MP BLOCK W125 54MM (STEEL)', '', ''),
(456, 'MP BLOCK W125 57MM SEMI TYPE (STEEL)', '', ''),
(457, 'MANIFOLD XRM 24MM', '', ''),
(458, 'BEAST TIRE 60X80X14 4PR FLASH P6240 TL', '', ''),
(459, 'BEAST TIRE 70X80X14 4PR FLASH P6240 TL', '', ''),
(460, 'BEAST TIRE 80X80X14 4PR FLASH P6240 TL', '', ''),
(461, 'BEAST TIRE 60X80X17 4PR FLASH P6240 TL', '', ''),
(462, 'BEAST TIRE 70X80X17 4PR FLASH P6240 TL', '', ''),
(463, 'BEAST TIRE 80X80X17 4PR FLASH P6240 TL', '', ''),
(464, 'BWIN PULLEY SET BEAT FI', '', ''),
(465, 'BWIN PULLEY SET CLICK 125 V2 NEW', '', ''),
(466, 'BWIN PULLEY SET M3 V2 NEW', '', ''),
(467, 'BWIN PULLEY SET NMAX V2 NEW', '', ''),
(468, 'BWIN PULLEY SET MIO', '', ''),
(469, 'BWIN CLUTCH BELL MIO', '', ''),
(470, 'BWIN CLUTCH BELL CLICK 125', '', ''),
(471, 'BWIN CLUTCH BELL NMAX', '', ''),
(472, 'BWIN CLUTCH BELL M3', '', ''),
(473, 'BWIN CLUTCH BELL BEAT FI', '', ''),
(474, 'SAIYAN RIM BROKEN 1.2/1.4X17 VIOLET', '', ''),
(475, 'SAIYAN RIM BROKEN 1.2/1.4X17 BROWN', '', ''),
(476, 'SAIYAN RIM BROKEN 1.2/1.4X17 PINK', '', ''),
(477, 'SAIYAN RIM BROKEN 1.2/1.4X17 GREEN', '', ''),
(478, 'SAIYAN RIM BROKEN 1.2/1.4X17 RED', '', ''),
(479, 'SAIYAN RIM BROKEN 1.2/1.4X17 LIME GREEN', '', ''),
(480, 'SAIYAN RIM BROKEN 1.2/1.4X17 ORANGE', '', ''),
(481, 'SAIYAN RIM BROKEN 1.2/1.4X17 GOLD', '', ''),
(482, 'BIG VALVE HEAD DASH 27/23MM MTRT', '', ''),
(483, 'THROTTLE BODY MIO 125 30MM MTRT', '', ''),
(484, 'THROTTLE BODY MIO 125 32MM MTRT', '', ''),
(485, 'MTRT FLY BALL MIO 6G', '', ''),
(486, 'MTRT FLY BALL MIO 8G', '', ''),
(487, 'MTRT FLY BALL MIOi 125 8G', '', ''),
(488, 'MTRT FLY BALL MIOi 125 11G', '', ''),
(489, 'MTRT FLY BALL MIOi 125 12G', '', ''),
(490, 'MTRT FLY BALL MIOi 125 14G', '', ''),
(491, 'CLUTCH CONVERTION KIT WAVE 100', '', ''),
(492, 'MTRT FLY BALL ZOOMER 8G', '', ''),
(493, 'MTRT FLY BALL ZOOMER 9G', '', ''),
(494, 'THROTTLE BODY NMAX 155 32MM MTRT', '', ''),
(495, 'THROTTLE BODY NMAX 155 34MM MTRT', '', ''),
(496, 'INJECTOR 6HOLES MIO 125 130CC MTRT', '', ''),
(497, 'MTRT TPS M3/MIO 125', '', ''),
(498, 'THROTTLE BODY AEROX 155 34MM', '', ''),
(499, 'PIPE CLICK11501 (SP)', '', ''),
(500, 'PIPE EVO-SP1 MIO', '', ''),
(501, 'MTRT TPS AEROX', '', ''),
(502, 'MTRT TPS NMAX 155', '', ''),
(503, 'MTRT TPS MXKING ', '', ''),
(504, 'FAN AEROX 155 YELLOW', '', ''),
(505, 'CLUTCH BELL AEROX 155 V2', '', ''),
(506, 'CLUTCH BELL MIO V2', '', ''),
(507, 'PULLEY SET MIO V2', '', ''),
(508, 'PULLEY SET V2 FOR NMAX 155', '', ''),
(509, 'CYLIN. HEAD NMAX 155 19/22MM', '', ''),
(510, 'DOWELL MIO', '', ''),
(511, 'NEEDLE BEARING MIO BLACK ', '', ''),
(512, 'NEEDLE BEARING MXKING', '', ''),
(513, 'CYLIN. HEAD AEROX 155 19/22MM', '', ''),
(514, 'REPAIR KIT CLICK', '', ''),
(515, 'QR CODES', '', ''),
(516, 'NMAX V2 SIDE POCKET', '', ''),
(517, 'PS16 RIGHT ', '', ''),
(518, 'PETRON FORK OIL ', '', ''),
(519, 'HERO HELMET HOOK GOLD', '', ''),
(520, 'T15 PARK LIGHT', '', ''),
(521, 'CAMSHAFT LHK W100', '', ''),
(522, 'BRT RACING CDI SPORTY', '', ''),
(523, 'BEAST TIRE 130X70X13', '', ''),
(524, 'INTERIOR ', '', ''),
(525, 'SLIDER YAMAHA SETS', '', ''),
(526, 'SLIDER HONDA click', '', ''),
(527, 'SPEED SENSOR HONDA BEAT ', '', ''),
(528, 'M3 REGULATOR', '', ''),
(529, 'CKP SENSOR AEROX', '', ''),
(530, 'DIAPHRAM SPORTY', '', ''),
(531, 'MAKOTO BRAKE SHOE BEAT', '', ''),
(532, 'DOMINO SWITCH AEROX LEFT RIGHT', '', ''),
(533, 'HAVOLIN RED UNDERBONE', '', ''),
(534, 'HAVOLIN YELLOW SCOOTER', '', ''),
(535, 'DELO GOLD', '', ''),
(536, 'TPS SENSOR BEAT FI', '', ''),
(537, 'ROCKER ARM MIO ROLLER', '', ''),
(538, 'CKP SENSOR BEAT FI', '', ''),
(539, 'PUSH PIN RIVETS', '', ''),
(540, 'HACHI BRAKESHOE TMX SUPREMO/BEAT', '', ''),
(541, 'HACHI C100/DREAM 34T CHAINSET', '', ''),
(542, 'HACHI  SMASH 36T CHAIN SET', '', ''),
(543, 'HACHI RAIDER 150 OLD CHAINSET', '', ''),
(544, 'HACHI BRAKESHOE TMX RR', '', ''),
(545, 'HACHI BRAKESHOE B120/B1LP/HD3 RR', '', ''),
(546, 'HACHI BRAKESHOE CT100', '', ''),
(547, 'HACHI BRAKESHOE TMX FR', '', ''),
(548, 'HACHI CHAIN SET RAIDER 150 NEW', '', ''),
(549, 'HACHI BRAKESHOE BARAKO', '', ''),
(550, 'HACHI BRAKESHOE MIO ', '', ''),
(551, ' JRP RUBBERIZED SEAT COVER MEDIUM', '', ''),
(552, 'JRP RUBBERIZED SEAT COVER NMAX ', '', ''),
(553, 'JRP RUBBERIZED SEAT COVER AEROX ', '', ''),
(554, 'JRP RUBBERIZED SEAT COVER SMALL', '', ''),
(555, 'OHLINS SHOCK 300MM', '', ''),
(556, 'KYB PREMIUM SHOCK BLACK 318MM', '', ''),
(557, 'KYB PREMIUM SHOCK WHITE 318MM', '', ''),
(558, 'KYB PREMIUM SHOCK YELLOW 318MM', '', ''),
(559, 'POWER TIRE 90/90', '', ''),
(560, 'SHELL OIL/GEAR OIL', '', ''),
(561, 'DLH DRIVE FACE', '', ''),
(562, 'PLATINUM XRACE 800ML', '', ''),
(563, 'SPEED SAE 4T', '', ''),
(564, 'DURO 2T', '', ''),
(565, 'BALL RACE RAIDER 150', '', ''),
(566, 'BRAKE SHOE CLICK', '', ''),
(567, 'EARLS HOSE FRONT', '', ''),
(568, 'APR SEAT COVER MAY TAHI CLICK NMAX', '', ''),
(569, 'APR SEAT COVER MAY TAHI SMALL SNIPER ', '', ''),
(570, 'APR SEAT MAY TAHI MEDIUM', '', ''),
(571, 'WD 40 333ML', '', ''),
(572, 'SHELL OIL BLUE 1L', '', ''),
(573, 'FLAT BAR ', '', ''),
(574, 'TIMING CHAIN COVER ORING MIO SPORTY', '', ''),
(575, 'BRAKE LIGHT SWITCH MIO RIGHT', '', ''),
(576, 'BRAKE LIGHT SWITCH MIO LEFT', '', ''),
(577, 'TAPPET SCREW ', '', ''),
(578, 'EXHAUST GASKET MIO', '', ''),
(579, 'WIRE BLACK RED', '', ''),
(580, 'CAMSHAFT BEARING MIO SPORTY', '', ''),
(581, 'CAMSHAFT BEARING BEAT FI CARB', '', ''),
(582, 'CAMSHAFT BEARING W125 W100', '', ''),
(583, 'CAMSHAFT BEARING NMAX AEROX', '', ''),
(584, 'CLUTCH LINING BEAT FI', '', ''),
(585, 'MAGNETO COIL MIO ', '', ''),
(586, 'REGULATOR MIO STD', '', ''),
(587, 'STARTER BEAD XRM', '', ''),
(588, 'STARTER BEAD MIO ', '', ''),
(589, 'TORQUE DRIVE MIO SPORTY', '', ''),
(590, 'PULLY SET MIO SPORTY L9', '', ''),
(591, 'MAGNETO COILS ', '', ''),
(592, 'L9 MANIFOLD MIO SPORTY', '', ''),
(593, 'CLUTCH LINING GY6125', '', ''),
(594, 'CLUTCH LINING SKYDRIVE', '', ''),
(595, ' CAMSHAFT MIO L9', '', ''),
(596, 'CLUTCH LINING MIO SPORTY', '', ''),
(597, 'CAMSHAFT BEARING M3', '', ''),
(598, 'V1 LIGHTEN DISC RAIDER150', '', ''),
(599, ' INTERRUPTOR RELAY', '', ''),
(600, 'LIGHTEN FRONT SHOCK MIO ', '', ''),
(601, 'DOMINO LEVER ORDINARY', '', ''),
(602, 'HYLOS CLICK 17S', '', ''),
(603, 'WAVE125 TENSIONER', '', ''),
(604, 'MIO TENSIONER', '', ''),
(605, 'WAVE125 TAPPET COVER', '', ''),
(606, 'WAVE100 TAPPET COVER', '', ''),
(607, 'MIO TAPPET COVER', '', ''),
(608, 'MHR CLICK CENTER SPRING 1500RPM', '', ''),
(609, 'MHR BEAT FI CENTER SPRING 1000RPM', '', ''),
(610, 'MHR NMAX/AEROX CENTER SPRING 1500RPM', '', ''),
(611, 'MHR CLICK CENTER SPRING 1000RPM', '', ''),
(612, 'MHR BEAT FI CENTER SPRING 1500RPM', '', ''),
(613, 'MHR MIO M3 CENTER SPRING 1500RPM', '', ''),
(614, 'MHR NMAX/AEROX CENTER SPRING 1000RPM', '', ''),
(615, 'MHR NMAX/AEROX CENTER SPRING 1200RPM', '', ''),
(616, 'MHR MIO M3 CENTER SPRING 1200RPM', '', ''),
(617, 'MHR CLICK 125 YELLOW 1200RPM', '', ''),
(618, 'MHR MIO M3 CENTER SPRING 1000RPM', '', ''),
(619, 'SPYDER FRONT SHOCK XRM', '', ''),
(620, 'AEROX GENUINE', '', ''),
(621, 'M3 GENUINE', '', ''),
(622, 'YAMAHA 54P INJECTOR TUBE ', '', ''),
(623, 'DS4 R CLICK BLACK', '', ''),
(624, 'DS4 R NMAX V1 BLACK', '', ''),
(625, 'DS4 R AEROX V1 CLEAR', '', ''),
(626, 'DS4 AEROX V2 BLACK', '', ''),
(627, 'DS4 R CLICK CLEAR', '', ''),
(628, 'DS4 R AEROX V1 BLACK', '', ''),
(629, 'DS4 R NMAX V1 CLEAR', '', ''),
(630, 'DS4 R NMAX V2 BLACK ', '', ''),
(631, 'DS4 R NMAX V2 CLEAR', '', ''),
(632, 'DS4 R AEROX V2 CLEAR', '', ''),
(633, 'SENSOR ASSY OIL TEMPERATURE BEAT FI HONDA', '', ''),
(634, 'OIL SEAL CLICK 125 150 HONDA LEFT CRANKCASE KWN 901', '', ''),
(635, 'OIL SEAL PULLEY SIDE MIO I 125 54PI', '', ''),
(636, 'AIR FILTER YAMAHA MIO GEAR GRAVIS 125', '', ''),
(637, 'DIO THROTTLE CABLE ', '', ''),
(638, 'STARTER RELAY CLICK ', '', ''),
(639, 'STARTER RELAY BEAT FI', '', ''),
(640, 'HAZZARD SWITCH', '', ''),
(641, 'HORN SWITCH ORDINARY', '', ''),
(642, 'STARTER SWITCH ORDINARY', '', ''),
(643, 'BRAKE PAD MIO I 125', '', ''),
(644, 'BREMBO CLUTCH LEVER LEFT ONLY', '', ''),
(645, 'CARB PISTON KIT 24MM', '', ''),
(646, 'CARB PISTON KIT 26MM', '', ''),
(647, 'CARB PISTON KIT 28MM', '', ''),
(648, 'FLOATING DISC 4 HOLES 200MM MIO GOLD SILVER', '', ''),
(649, 'T15 DC ICE BLUE', '', ''),
(650, 'T15 DC BLUE', '', ''),
(651, 'T15 DC RED', '', ''),
(652, 'T15 DC WHITE', '', ''),
(653, 'CNC AXLE WAVE FRONT', '', ''),
(654, 'NMAX AEROX MDL BRACKET ONLY', '', ''),
(655, 'FRONT SHOCK OIL SEAL XRM', '', ''),
(656, 'BOSNY SPRAY PAINT', '', ''),
(657, 'RP BRAKE SHOE', '', ''),
(658, 'YAMAKOTO BRAKE SHOE MIO', '', ''),
(659, 'DUAL CONTACT ', '', ''),
(660, 'BREMBO DISC GOLD SILVER 200MM', '', ''),
(661, 'SAIYAN ROCKER ARM M3', '', ''),
(662, 'SAIYAN CDI TMX ALPHA', '', ''),
(663, 'SAIYAN CDI BARAKO', '', ''),
(664, 'SAIYAN CDI WAVE125 OLD', '', ''),
(665, 'SAIYAN CDI MIO SOULTY', '', ''),
(666, 'SAIYAN CDI WAVE125 ALPHA', '', ''),
(667, 'SAIYAN CDI TMX155', '', ''),
(668, 'SAIYAN COLORED CHAIN 428 130', '', ''),
(669, 'SAIYAN CLUTCH LINING CB125', '', ''),
(670, 'SAIYAN CLUTCH LINING CT100', '', ''),
(671, 'SAIYAN CLUTCH LINING FZ16', '', ''),
(672, 'SAIYAN CLUTCH LINING ROUSER135', '', ''),
(673, 'SAIYAN CLUTCH LINING RUSI/TC150', '', ''),
(674, 'SAIYAN CLUTCH LINING SMASH', '', ''),
(675, 'SAIYAN CLUTCH LINING TMX', '', ''),
(676, 'SAIYAN CLUTCH LINING WAVE125', '', ''),
(677, 'SAIYAN CLUTCH LINING RAIDER150', '', ''),
(678, 'SAIYAN CLUTCH LINING BARAKO', '', ''),
(679, 'SAIYAN CLUTCH LINING HD3', '', ''),
(680, 'SAIYAN CLUTCH LINING CT150', '', ''),
(681, 'SAIYAN CLUTCH LINING XRM110', '', ''),
(682, 'SAIYAN REGULATOR GY6 5WIRES', '', ''),
(683, 'SAIYAN REGULATOR M3', '', ''),
(684, 'SAIYAN REGULATOR TMX SUPREMO', '', ''),
(685, 'SAIYAN REGULATOR WAVE125', '', ''),
(686, 'SAIYAN REGULATOR TMX ALPHA', '', ''),
(687, 'SAIYAN ANTI THEFT SWITCH SET MIO SPORTY', '', ''),
(688, 'SAIYAN ANTI THEFT SWITCH SET RAIDER CARB', '', ''),
(689, 'SAIYAN ANTI THEFT SWITCH SET SMASH110', '', ''),
(690, 'SAIYAN ANTI THEFT SWITCH SET SMASH115 (DISC)', '', ''),
(691, 'SAIYAN ANTI THEFT SWITCH SET BEAT', '', ''),
(692, 'SAIYAN STARTER RELAY RAIDER150', '', ''),
(693, 'SAIYAN STARTER RELAY TMX', '', ''),
(694, 'SAIYAN MAIN SWITCH WAVE125', '', ''),
(695, 'SAIYAN MAIN SWITCH SMASH', '', ''),
(696, 'SAIYAN MAIN SWITCH CT100', '', ''),
(697, 'SAIYAN MAIN SWITCH HD3', '', ''),
(698, 'SAIYAN MAIN SWITCH MIO SPORTY/MIO OLD', '', ''),
(699, 'SAIYAN MAIN SWITCH TMX SUPREMO', '', ''),
(700, 'SAIYAN MAIN SWITCH C100/DREAM', '', ''),
(701, 'SAIYAN MAIN SWITCH SNIPER135', '', ''),
(702, 'SAIYAN MAIN SWITCH CT150 BOXER', '', ''),
(703, 'SAIYAN MAIN SWITCH TMX155', '', ''),
(704, 'SAIYAN MINI DRIVING LIGHT SS3', '', ''),
(705, 'SAIYAN MINI DRIVING LIGHT SS4', '', ''),
(706, 'SAIYAN FRONT SHOCK ASSY BARAKO', '', ''),
(707, 'SAIYAN FRONT SHOCK ASSY XRM 125', '', ''),
(708, 'SAIYAN CHAINSET XRM 14 34 110L', '', ''),
(709, 'SAIYAN CHAINSET SMASH 14 36 110L', '', ''),
(710, 'SAIYAN CHAINSET BARAKO 14 45 120L', '', ''),
(711, 'SAIYAN CHAINSET TMX 14 36 110L', '', ''),
(712, 'SAIYAN CHAINSET C100 14 34 110L', '', ''),
(713, 'SAIYAN CHAIN GUIDE MIO', '', ''),
(714, 'SAIYAN CHAIN GUIDE RAIDER150', '', ''),
(715, 'SAIYAN CHAIN GUIDE WAVE125', '', ''),
(716, 'SAIYAN BRAKE SHOE TMX125 ALPHA', '', ''),
(717, 'SAIYAN BRAKE SHOE TMX155 REAR', '', ''),
(718, 'SAIYAN BRAKE SHOE BARAKO REAR', '', ''),
(719, 'SAIYAN BRAKE SHOE SKYDRIVE', '', ''),
(720, 'SAIYAN BRAKE PAD RAIDER150 FRONT', '', ''),
(721, 'SAIYAN BRAKE PAD RAIDER150 REAR', '', ''),
(722, 'SAIYAN BRAKE PAD MIO', '', ''),
(723, 'SAIYAN BRAKE PAD M3', '', ''),
(724, 'SAIYAN BRAKE PAD WAVE125', '', ''),
(725, 'SAIYAN BRAKE PAD PCX160/ADV150 REAR', '', ''),
(726, 'SAIYAN BRAKE PAD PCX160/ADV150 FRONT', '', ''),
(727, 'SAIYAN BRAKE PAD CLICK125', '', ''),
(728, 'SAIYAN BRAKE PAD WAVE110', '', ''),
(729, 'SAIYAN BRAKE PAD TRINITY', '', ''),
(730, 'SAIYAN BRAKE PAD BEAT NEW', '', ''),
(731, 'SAIYAN FUEL FILTER CLICK150', '', ''),
(732, 'SAIYAN FUEL FILTER AEROX', '', ''),
(733, 'SAIYAN FUEL FILTER RAIDER150 FI', '', ''),
(734, 'SAIYAN TORQUE DRIVE BEAT SCOOPY', '', ''),
(735, 'SAIYAN TORQUE DRIVE MIO I 125/MIO SOUL I 125', '', ''),
(736, 'SAIYAN TORQUE DRIVE MIO SOUL I 115', '', ''),
(737, 'SAIYAN TORQUE DRIVE GY6', '', ''),
(738, 'SAIYAN TORQUE DRIVE NMAX', '', ''),
(739, 'SAIYAN TORQUE DRIVE PCX2020 (K97)', '', ''),
(740, 'SAIYAN CLUTCH SHOE ASSY BEAT SCOOPY', '', ''),
(741, 'SAIYAN CLUTCH SHOE ASSY CLICK125', '', ''),
(742, 'SAIYAN CLUTCH SHOE ASSY CLICK150', '', ''),
(743, 'SAIYAN CLUTCH SHOE ASSY MIO I 125/MIO SOUL I 125', '', ''),
(744, 'SAIYAN CLUTCH SHOE ASSY MIO SOUL I 115', '', ''),
(745, 'SAIYAN CLUTCH SHOE ASSY ZOOMER-X/BEAT FI', '', ''),
(746, 'SAIYAN CLUTCH SHOE ASSY GY6', '', ''),
(747, 'SAIYAN CLUTCH SHOE ASSY PCX2020 (K97)', '', ''),
(748, 'SAIYAN CLUTCH SHOE W/ CLUTCH SPRING BEAT SCOOPY', '', ''),
(749, 'SAIYAN CLUTCH SHOE W/ CLUTCH SPRING PCX2020 (K97)', '', ''),
(750, 'SAIYAN CLUTCH SHOE W/ CLUTCH SPRING SKYDRIVE', '', ''),
(751, 'SAIYAN CLUTCH SHOE W/ CLUTCH SPRING MIO SOUL I 115', '', ''),
(752, 'SAIYAN CLUTCH SHOE W/ CLUTCH SPRING GY6', '', ''),
(753, 'SAIYAN CLUTCH SHOE W/ CLUTCH SPRING NMAX', '', ''),
(754, 'SAIYAN CLUTCH SHOE W/ CLUTCH SPRING CLICK150', '', ''),
(755, 'SAIYAN CLUTCH SHOE W/ CLUTCH SPRING MIO I 125/MIO SOUL I 125', '', ''),
(756, 'SAIYAN PULLEY SET ASSY W/ DRIVE FACE MIO I 125/MIO SOUL I 125', '', ''),
(757, 'SAIYAN PULLEY SET ASSY W/ DRIVE FACE SKYDRIVE', '', ''),
(758, 'SAIYAN PULLEY SET ASSY W/ DRIVE FACE ZOOMER-X/BEAT FI', '', ''),
(759, 'SAIYAN PULLEY SET W/ PIN (NO DRIVE FACE) MIO', '', ''),
(760, 'SAIYAN PULLEY SET W/ PIN (NO DRIVE FACE) CLICK125', '', ''),
(761, 'SAIYAN PULLEY SET W/ PIN (NO DRIVE FACE) CLICK150', '', ''),
(762, 'SAIYAN PULLEY SET W/ PIN (NO DRIVE FACE) MIO I 125/MIO SOUL I 125', '', ''),
(763, 'SAIYAN PULLEY SLIDER PIECE GY6', '', ''),
(764, 'K36 BELT CLICK', '', ''),
(765, 'K44 BELT BEAT FI', '', ''),
(766, 'GY6 125 BELT ', '', ''),
(767, 'NATHONG SEAT MIO 125', '', ''),
(768, 'BREMBO RIGHT TANK ', '', ''),
(769, 'RS8 PULLEY SET SPORTY', '', ''),
(770, 'RS8 CLUTCH LINING SPORTY ', '', ''),
(771, 'SUPER SPEED ENGINE SUPPORT', '', ''),
(772, 'SAIYAN BEARING 6300', '', ''),
(773, 'SAIYAN SPOKES 10GX162', '', ''),
(774, 'MIO LOGO EMBLEM', '', ''),
(775, 'NMAX LOGO SET EMBLEM', '', ''),
(776, 'BREMBO MOUSE 4POT', '', ''),
(777, 'COCKROACH 4POT', '', ''),
(778, 'PRC COLORED CHAIN', '', ''),
(779, 'JRP HANDLE GRIP', '', ''),
(780, 'VOLT METER', '', ''),
(781, 'CLICK LEVER SET', '', ''),
(782, 'SUPER SPEED GEAR OIL', '', ''),
(783, 'BRAKE PAD CLICK RZ', '', ''),
(784, 'BRAKE PAD BEAT RZ', '', ''),
(785, 'BANTALAN BOLA KVB-900', '', ''),
(786, 'PIPE BRACKET ', '', ''),
(787, 'ZENO FLAT SEAT CLICK', '', ''),
(788, 'ZENO FLAT SEAT MIO SOULTY', '', ''),
(789, 'ZENO FLAT SEAT AEROX', '', ''),
(790, 'ZENO FLAT SEAT RAIDER', '', ''),
(791, 'ZENO FLAT SEAT PCX', '', ''),
(792, 'ZION FORK OIL', '', ''),
(793, 'BRONZEBREMBO CALIPER 2POT', '', ''),
(794, 'WAVE125 LIGHTEN FRONT SHOCK', '', ''),
(795, 'SPEEDOMETER SENSOR BEAT FI', '', ''),
(796, 'BLUE CORE', '', ''),
(797, 'BENDIX MIO L9', '', ''),
(798, 'MTRT DIO/BEAT FLYBALL 6GRAMS', '', ''),
(799, 'MTRT DIO/BEAT FLYBALL 7GRAMS', '', ''),
(800, 'MTRT DIO/BEAT FLYBALL 8GRAMS', '', ''),
(801, 'MTRT DIO/BEAT FLYBALL 9GRAMS', '', ''),
(802, 'MTRT DIO/BEAT FLYBALL 10GRAMS', '', ''),
(803, 'MTRT DIO/BEAT FLYBALL 11GRAMS', '', ''),
(804, 'MTRT DIO/BEAT FLYBALL 12GRAMS', '', ''),
(805, 'MTRT DIO/BEAT FLYBALL 13GRAMS', '', ''),
(806, 'MTRT GY6/BEAT FI FLYBALL 8GRAMS', '', ''),
(807, 'MTRT GY6/BEAT FI FLYBALL 10GRAMS', '', ''),
(808, 'MTRT GY6/BEAT FI FLYBALL 11GRAMS', '', ''),
(809, 'MTRT GY6/BEAT FI FLYBALL 12GRAMS', '', ''),
(810, 'MTRT GY6/BEAT FI FLYBALL 13GRAMS', '', ''),
(811, 'MTRT GY6/BEAT FI FLYBALL 14GRAMS', '', ''),
(812, 'MTRT GY6/BEAT FI FLYBALL 15GRAMS', '', ''),
(813, 'MTRT MIO/SOULI115/NOUVO FLYBALL 6GRAMS', '', ''),
(814, 'MTRT MIO/SOULI115/NOUVO FLYBALL 6.5GRAMS', '', ''),
(815, 'MTRT MIO/SOULI115/NOUVO FLYBALL 7GRAMS', '', ''),
(816, 'MTRT MIO/SOULI115/NOUVO FLYBALL 7.5GRAMS', '', ''),
(817, 'MTRT MIO/SOULI115/NOUVO FLYBALL 8.5GRAMS', '', ''),
(818, 'MTRT MIO/SOULI115/NOUVO FLYBALL 9.5GRAMS', '', ''),
(819, 'MTRT MIO/SOULI115/NOUVO FLYBALL 10GRAMS', '', ''),
(820, 'MTRT MIO/SOULI115/NOUVO FLYBALL 11GRAMS', '', ''),
(821, 'MTRT MIO/SOULI115/NOUVO FLYBALL 12GRAMS', '', ''),
(822, 'MTRT NMAX/AEROX/MIO125 FLYBALL 7GRAMS', '', ''),
(823, 'MTRT NMAX/AEROX/MIO125 FLYBALL 8GRAMS', '', ''),
(824, 'MTRT NMAX/AEROX/MIO125 FLYBALL 9GRAMS', '', ''),
(825, 'MTRT NMAX/AEROX/MIO125 FLYBALL 9.5GRAMS', '', ''),
(826, 'MTRT NMAX/AEROX/MIO125 FLYBALL 10GRAMS', '', ''),
(827, 'MTRT NMAX/AEROX/MIO125 FLYBALL 10.5GRAMS', '', ''),
(828, 'MTRT NMAX/AEROX/MIO125 FLYBALL 12GRAMS', '', ''),
(829, 'MTRT NMAX/AEROX/MIO125 FLYBALL 13GRAMS', '', ''),
(830, 'MTRT NMAX/AEROX/MIO125 FLYBALL 14GRAMS', '', ''),
(831, 'MTRT NMAX/AEROX/MIO125 FLYBALL 15GRAMS', '', ''),
(832, 'MTRT ADV150/PCX FLYBALL 17GRAMS', '', ''),
(833, 'MTRT ADV150/PCX FLYBALL 18GRAMS', '', ''),
(834, 'MTRT ADV150/PCX FLYBALL 19GRAMS', '', ''),
(835, 'MTRT CLICK125/DINK/SKYDRIVE FLYBALL 7GRAMS', '', ''),
(836, 'MTRT CLICK125/DINK/SKYDRIVE FLYBALL 9GRAMS', '', ''),
(837, 'MTRT CLICK125/DINK/SKYDRIVE FLYBALL 10GRAMS', '', ''),
(838, 'MTRT CLICK125/DINK/SKYDRIVE FLYBALL 11GRAMS', '', ''),
(839, 'MTRT CLICK125/DINK/SKYDRIVE FLYBALL 12GRAMS', '', ''),
(840, 'MTRT CLICK125/DINK/SKYDRIVE FLYBALL 14GRAMS', '', ''),
(841, 'MTRT CLICK125/DINK/SKYDRIVE FLYBALL 15GRAMS', '', ''),
(842, 'MTRT CLICK125/DINK/SKYDRIVE FLYBALL 16GRAMS', '', ''),
(843, 'MTRT XMAX300 FLYBALL 14GRAMS', '', ''),
(844, 'MTRT XMAX300 FLYBALL 15GRAMS', '', ''),
(845, 'MTRT XMAX300 FLYBALL 16GRAMS', '', ''),
(846, 'MTRT XMAX300 FLYBALL 17GRAMS', '', ''),
(847, 'MTRT XMAX300 FLYBALL 18GRAMS', '', ''),
(848, 'CELLPHONE HOLDER', '', ''),
(849, 'BRAKE LEVER DOMINO MIO BLACK', '', ''),
(850, 'BRAKE LEVER DOMINO MIO CARBON', '', ''),
(851, 'BRAKE LEVER DOMINO MIO GOLD', '', ''),
(852, 'SIDE MIRROR ', '', ''),
(853, 'HMA SIDE MIRROR', '', ''),
(854, 'XRM/WAVE LEVER DOMINO', '', ''),
(855, 'HONEY WELL SWITCH', '', ''),
(856, 'KOYO 6300', '', ''),
(857, '5 WIRE FULLWAVE', '', ''),
(858, 'DAYWAY YTX4L-BS', '', ''),
(859, 'CARBON BRUSH ', '', ''),
(860, 'PUSH BUTTON SWITCH', '', ''),
(861, 'JRP GRIP BLACK', '', ''),
(862, 'SAIYAN BEARING 6303', '', ''),
(863, 'SAIYAN BEARING 6204', '', ''),
(864, 'SAIYAN BEARING 6201', '', ''),
(865, 'SAIYAN BEARING 6301', '', ''),
(866, 'YAGUSO RIOS 162', '', ''),
(867, 'THROTTLE CABLE BARAKO', '', ''),
(868, 'CLUTCH CABLE SHOGUN', '', ''),
(869, 'CLUTCH CABLE RAIDER 125', '', ''),
(870, 'CLUTCH CABLE BARAKO', '', ''),
(871, 'ZENO POWER PIPE WAVE125', '', ''),
(872, 'ZENO POWER PIPE BEAT', '', ''),
(873, 'ZENO POWER PIPE NMAX V2', '', ''),
(874, 'ZENO POWER PIPE CLICK', '', ''),
(875, 'TDD LED LIGHT', '', ''),
(876, 'SAIYAN THROTTLE CABLE WAVE 100 ', '', ''),
(877, 'GY6 BELT', '', ''),
(878, 'DOMINO HANDLE SWITCH ASSY SET MIO125/AEROX', '', ''),
(879, 'BAR END ', '', ''),
(880, 'SC SIAM V.1 GEARS CNC 6/20', '', ''),
(881, 'DOMINIO CABLE UNIVERSAL', '', ''),
(882, 'SPEEDOMETER PLATE NMAX/AEROX', '', ''),
(883, 'DUAL CONTACT YELLOW/WHITE', '', ''),
(884, 'SAIYAN HORN RELAY 4PIN NO SOCKET', '', ''),
(885, 'SAIYAN FLASHER RELAY ', '', ''),
(886, 'OD 4L BATTERY', '', ''),
(887, 'MIO AMORE FAIRINGS', '', ''),
(888, 'MIO 1 HEAD LIGHT / WINKER L/R', '', ''),
(889, 'BEAST TIRE 90X90X17 4PR FLASH P6240 TL 49P', '', ''),
(890, 'BEAST TIRE 70X90X17 4PR FLASH P6240 TL 38P', '', ''),
(891, 'BEAST TIRE 80X90X17 4PR FLASH P6240 TL 44P', '', ''),
(892, 'BEAST TIRE 60X90X17 4PR FLASH P6240 TL 30P', '', ''),
(893, 'BEAST TIRE 110X80X14 4PR FLASH P6240 TL 53S', '', ''),
(894, 'BEAST TIRE 140X70X14 4PR FLASH P6240 TL 62S', '', ''),
(895, 'BEAST TIRE 70X90X14 4PR FLASH P6240 TL 34P', '', ''),
(896, 'BEAST TIRE 80X90X14 4PR FLASH P6240 TL 40P', '', ''),
(897, 'BEAST TIRE 90X90X14 4PR FLASH P6240 TL 46P', '', ''),
(898, 'PODIUM PIPE MIO i 125', '', ''),
(899, 'PODIUM PIPE MIO SPORTY', '', ''),
(900, 'PODIUM PIPE CLICK 125/150', '', ''),
(901, 'PODIUM PIPE WAVE 100S', '', ''),
(902, 'RS8 DEGREASER', '', ''),
(903, 'MANUAL TENSIONER MIO', '', ''),
(904, 'MANUAL TENSIONER RAIDER', '', ''),
(905, 'MANUAL TENSIONER WAVE125', '', ''),
(906, 'SPEED SENSOR PLATE AEROX/NMAX', '', ''),
(907, 'LHK CAMSHAFT MIO SPORTY STAGE1', '', ''),
(908, 'LHK CAMSHAFT MIO SPORTY STAGE2', '', ''),
(909, 'LHK CAMSHAFT MIO SPORTY STAGE3', '', ''),
(910, 'BRAKE LIKE SWITCH HONDA', '', ''),
(911, 'APIDO CDI AMORE', '', ''),
(912, 'RIZOMA MINI DRIVING LIGHT', '', ''),
(913, 'APR SEAT COVER MAY TAHI SMALL BLACK', '', ''),
(914, 'APR SEAT COVER MAY TAHI RAIDER FI BLACK', '', ''),
(915, 'APR SEAT COVER MAY TAHI CLICK BLACK', '', ''),
(916, 'APR SEAT COVER MAY TAHI SMALL BLUE', '', ''),
(917, 'APR SEAT COVER MAY TAHI MEDIUM BLUE', '', ''),
(918, 'APR SEAT COVER MAY TAHI AEROX RED', '', ''),
(919, 'APR SEAT COVER MAY TAHI MEDIUM RED', '', ''),
(920, 'APR SEAT COVER MAY TAHI RAIDER FI RED', '', ''),
(921, 'CLICK 150 BRAKE MASTER REPAIR KIT', '', ''),
(922, 'RAM AIR CLICK 160', '', ''),
(923, 'CARB COVER MIO SPORTY', '', ''),
(924, 'HMA SIDE MIRROR HONDA', '', ''),
(925, 'HMA SIDE MIRROR YAMAHA', '', ''),
(926, 'DOMINO TRI SWITCH HONDA BEAT', '', ''),
(927, 'SAIYAN FRONT SHOCK WAVE 100 ', '', ''),
(928, 'SAIYAN FRONT SHOCK WAVE 125', '', ''),
(929, 'SAIYAN FRONT SHOCK CLICK 125 ', '', ''),
(930, 'SAIYAN FRONT SHOCK RAIDER150', '', ''),
(931, 'SAIYAN FRONT SHOCK RUSI TC 125', '', ''),
(932, 'SAIYAN FRONT SHOCK CT100', '', ''),
(933, 'SAIYAN FRONT SHOCK TMX 125', '', ''),
(934, 'OD 12N5L BATTERY', '', ''),
(935, 'FORK OIL  ZION', '', ''),
(936, 'NON VENTILATED DISC', '', ''),
(937, 'LAMBORG9 TORQUE DRIVE MIO SPORTY', '', ''),
(938, 'LAMBORG9 TORQUE DRIVE MIO I 125', '', ''),
(939, 'LAMBORG9 TORQUE DRIVE BEAT FI', '', ''),
(940, 'LAMBORG9 VALVE GUIDE SMASH', '', ''),
(941, 'LAMBORG9 VALVE GUIDE XRM', '', ''),
(942, 'LAMBORG9 VALVE GUIDE SHOGUN', '', ''),
(943, 'LAMBORG9 VALVE GUIDE GY6 125', '', ''),
(944, 'LAMBORG9 VALVE GUIDE MIO', '', ''),
(945, 'LAMBORG9 VALVE GUIDE TMX', '', ''),
(946, 'LAMBORG9 VALVE GUIDE R. 150', '', ''),
(947, 'PETRON GEAR OIL', '', ''),
(948, 'R. 150 CAM CHAIN GUIDE', '', ''),
(949, 'TRANSFORMER x KHOKEN BRAKE', '', ''),
(950, 'CIRCUIT AUTOMOTIVE WIRE ', '', ''),
(951, 'LAMBORG9 STARTER BEAD MIO', '', ''),
(952, 'LAMBORG9 CAMSHAFT STD MIO', '', ''),
(953, 'LAMBORG9 CAMSHAFT STD XRM 110', '', ''),
(954, 'LAMBORG CAM BEARING MIO SPORTY', '', ''),
(955, 'LAMBORG CAM BEARING MIO I 125', '', ''),
(956, 'LAMBORG CAM BEARING W125/W100', '', ''),
(957, 'DS4 RADIATOR COVER CLICK BLACK/CLEAR', '', ''),
(958, 'DS4 RADIATOR COVER AEROX V1 V2 BLACK/CLEAR', '', ''),
(959, 'DS4 RADIATOR COVER MXI BLACK/CLEAR', '', ''),
(960, 'DS4 RADIATOR COVER NMAX V1 V2 BLACK/CLEAR', '', ''),
(961, 'OIL FILTER KAWASAKI BARAKO', '', ''),
(962, 'OIL FILTER KAWASAKI BAJAJ', '', ''),
(963, 'CNC CRANKCASE BOLT GEAR TYPE W100/W110', '', ''),
(964, 'CNC CRANKCASE BOLT GEAR TYPE SNIPER 150', '', ''),
(965, 'HONDA INJECTOR', '', ''),
(966, 'OHLINS STABILIZER', '', ''),
(967, 'CNC BODY BOLT V1 GEAR TYPE 6X20', '', ''),
(968, 'CRANK CASE CNC BOLT PCX', '', ''),
(969, 'CRANK CASE CNC BOLT CLICK', '', ''),
(970, 'FLOATING DISC 4 HOLES 220MM MIO SILVER/BLUE', '', ''),
(971, 'CNC BOLTS WHEEL SHAFTING  AEROX V1  FRONT', '', ''),
(972, 'CNC BOLTS WHEEL SHAFTING  CLICK/BEAT FRONT', '', ''),
(973, 'CNC BOLTS WHEEL SHAFTING WAVE REAR STD.', '', ''),
(974, 'CNC BOLTS WHEEL SHAFTING  WAVE LONG', '', ''),
(975, 'CNC BOLTS WHEEL SHAFTING  NMAX V1', '', ''),
(976, 'CNC BOLTS WHEEL SHAFTING  SPORTY/ SNIPER', '', ''),
(977, 'CNC BOLTS WHEEL SHAFTING SONIC/RAIDER', '', ''),
(978, 'CNC REAR AXLE NUT PCX/CLICK/ADV', '', ''),
(979, 'CNC REAR AXLE NUT MIO/M3', '', ''),
(980, 'SAIYAN SLIDER PEACE MIO/NMAX', '', ''),
(981, 'SAIYAN SLIDER PEACE SKYDRIVE', '', ''),
(982, 'SAIYAN SLIDER PEACE CLICK', '', ''),
(983, 'SAIYAN SLIDER PEACE GY6', '', ''),
(984, 'SAIYAN SLIDER PEACE SCOOPY', '', ''),
(985, 'DORAEMON CALIPER W/ BRACKET CLICK/BEAT', '', ''),
(986, 'TURTLE BACK CALIPER W/ CNC BRACKET W125', '', ''),
(987, 'GP COCKROACH GP CALIPER W125', '', ''),
(988, 'GP COCKROACH GP CALIPER MIO', '', ''),
(989, 'MINI GP CALIPER W/ ALLOY BRACKET MIO/W125', '', ''),
(990, 'L9 CNC BAR END STAINLESS THAI MADE', '', ''),
(991, 'KING OF DRAG OUTER TUBE W125', '', ''),
(992, 'KING OF DRAG OUTER TUBE MIO', '', ''),
(993, 'KOYO 6000', '', ''),
(994, 'KOYO 63/22', '', ''),
(995, 'BALLRACE W125/CLICK', '', ''),
(996, 'MAGIC STAR MDL TRISWITCH', '', ''),
(997, 'MIO 1 KAHA', '', ''),
(998, 'CK CALIPER 2POT CLICK W/ BRACKET', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL,
  `service` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service`) VALUES
(1, 'CVT Cleaning'),
(2, 'CVT Refresh'),
(3, 'Diagnostic'),
(4, 'Change Oil'),
(5, 'Fi Cleaning'),
(6, 'Reset ECU'),
(7, 'Tune Up'),
(8, 'Shock Tuning'),
(9, 'Carb Cleaning'),
(10, 'Engine Upgrade'),
(11, 'Major Overhaul'),
(12, 'Top Overhaul');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_record`
--

CREATE TABLE `supplier_record` (
  `code` varchar(11) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_address` varchar(255) NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `contact_number` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `code` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `contact` varchar(50) NOT NULL,
  `usertype` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`code`, `firstname`, `email`, `password`, `contact`, `usertype`) VALUES
(1, 'Raine Rodriguez', 'rodriguezraine84@gmail.com', 'JennieKim@1', '09917165718', 'Super Admin');

-- --------------------------------------------------------

--
-- Table structure for table `users_branch`
--

CREATE TABLE `users_branch` (
  `code` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `contact` varchar(50) NOT NULL,
  `usertype` varchar(50) NOT NULL,
  `branch_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users_branch`
--

INSERT INTO `users_branch` (`code`, `firstname`, `email`, `password`, `contact`, `usertype`, `branch_code`) VALUES
(1, 'Sheena Jane Toroy', 'toroysheena@gmail.com', 'test123', '09123456789', 'Branch Manager', 'K1'),
(2, 'Juan Tamad', 'juantamad@gmail.com', 'test123', '091223456789', 'Branch Manager', 'K2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branch_record`
--
ALTER TABLE `branch_record`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `dealer_record`
--
ALTER TABLE `dealer_record`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `inflow_admin`
--
ALTER TABLE `inflow_admin`
  ADD KEY `fk_barcode` (`barcode`),
  ADD KEY `fk_codeINFLOW` (`code`);

--
-- Indexes for table `inflow_branch`
--
ALTER TABLE `inflow_branch`
  ADD KEY `fk_barcode_branch` (`barcode`),
  ADD KEY `fk_inflow_branch` (`code`);

--
-- Indexes for table `mechanic_record`
--
ALTER TABLE `mechanic_record`
  ADD PRIMARY KEY (`mechanic_id`),
  ADD KEY `fk_users_branch_code` (`user_branch_code`);

--
-- Indexes for table `mechanic_services`
--
ALTER TABLE `mechanic_services`
  ADD PRIMARY KEY (`mechanic_service_id`),
  ADD KEY `fk_mechanic_id` (`mechanic_id`),
  ADD KEY `fk_services_id` (`services_id`),
  ADD KEY `fk_mechanic_code` (`code`);

--
-- Indexes for table `outflow_selling`
--
ALTER TABLE `outflow_selling`
  ADD KEY `fk_barcode_outflow` (`barcode`),
  ADD KEY `fk_codeOUTFLOW` (`code`);

--
-- Indexes for table `outflow_selling_branch`
--
ALTER TABLE `outflow_selling_branch`
  ADD KEY `fk_barcode_outflow_branch` (`barcode`),
  ADD KEY `fk_codeOUTFLOW_branch` (`code`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`barcode`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `supplier_record`
--
ALTER TABLE `supplier_record`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `users_branch`
--
ALTER TABLE `users_branch`
  ADD PRIMARY KEY (`code`),
  ADD KEY `fk_branch` (`branch_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mechanic_record`
--
ALTER TABLE `mechanic_record`
  MODIFY `mechanic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mechanic_services`
--
ALTER TABLE `mechanic_services`
  MODIFY `mechanic_service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `barcode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users_branch`
--
ALTER TABLE `users_branch`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `inflow_admin`
--
ALTER TABLE `inflow_admin`
  ADD CONSTRAINT `fk_barcode` FOREIGN KEY (`barcode`) REFERENCES `products` (`barcode`),
  ADD CONSTRAINT `fk_codeINFLOW` FOREIGN KEY (`code`) REFERENCES `users` (`code`);

--
-- Constraints for table `inflow_branch`
--
ALTER TABLE `inflow_branch`
  ADD CONSTRAINT `fk_barcode_branch` FOREIGN KEY (`barcode`) REFERENCES `products` (`barcode`),
  ADD CONSTRAINT `fk_inflow_branch` FOREIGN KEY (`code`) REFERENCES `branch_record` (`code`);

--
-- Constraints for table `mechanic_record`
--
ALTER TABLE `mechanic_record`
  ADD CONSTRAINT `fk_users_branch_code` FOREIGN KEY (`user_branch_code`) REFERENCES `users_branch` (`code`);

--
-- Constraints for table `mechanic_services`
--
ALTER TABLE `mechanic_services`
  ADD CONSTRAINT `fk_mechanic_code` FOREIGN KEY (`code`) REFERENCES `users_branch` (`code`),
  ADD CONSTRAINT `fk_mechanic_id` FOREIGN KEY (`mechanic_id`) REFERENCES `mechanic_record` (`mechanic_id`),
  ADD CONSTRAINT `fk_services_id` FOREIGN KEY (`services_id`) REFERENCES `services` (`service_id`);

--
-- Constraints for table `outflow_selling`
--
ALTER TABLE `outflow_selling`
  ADD CONSTRAINT `fk_barcode_outflow` FOREIGN KEY (`barcode`) REFERENCES `products` (`barcode`),
  ADD CONSTRAINT `fk_codeOUTFLOW` FOREIGN KEY (`code`) REFERENCES `users` (`code`);

--
-- Constraints for table `outflow_selling_branch`
--
ALTER TABLE `outflow_selling_branch`
  ADD CONSTRAINT `fk_barcode_outflow_branch` FOREIGN KEY (`barcode`) REFERENCES `products` (`barcode`),
  ADD CONSTRAINT `fk_codeOUTFLOW_branch` FOREIGN KEY (`code`) REFERENCES `users_branch` (`code`);

--
-- Constraints for table `users_branch`
--
ALTER TABLE `users_branch`
  ADD CONSTRAINT `fk_branch` FOREIGN KEY (`branch_code`) REFERENCES `branch_record` (`code`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
