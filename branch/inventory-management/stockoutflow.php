<!DOCTYPE html>
<?php 
include('../../connection.php');
session_start();

if(isset($_SESSION["code"]))
{
    $code = $_SESSION["code"];
}else{
    echo"Record not Found!";
    exit;
}

$name = "SELECT firstname, usertype FROM users_branch WHERE code = $code";
$name_query = mysqli_query($conn, $name);

if(mysqli_num_rows($name_query) > 0){
    $row = mysqli_fetch_assoc($name_query);

    $fullname = $row["firstname"];
    $usertype = $row["usertype"];
}

date_default_timezone_set('Asia/Manila');
$product_query = "SELECT DISTINCT
                    o.date,
                    p.productname,
                    o.selling_price,
                    o.units_sold,
                    i.supplier_price,
                    i.totalvalue,
                    o.total_value_selling,
                    o.total_value_supplier,
                    o.profit,
                    o.discount,
                    o.total_profit
                    FROM
                    outflow_selling_branch o
                    JOIN
                    inflow_branch i ON o.barcode = i.barcode
                    JOIN
                    products p ON o.barcode = p.barcode
                    WHERE o.code = $code AND DATE(o.date) = CURDATE()";
$product_result = mysqli_query($conn, $product_query);

$productname = "";

if (isset($_POST['barcode'])) {
    $barcode = mysqli_real_escape_string($conn, $_POST['barcode']);

    $product = "SELECT products.productname FROM products 
                JOIN inflow_branch ON products.barcode = inflow_branch.barcode
                WHERE products.barcode = '$barcode'";
    $product_query = mysqli_query($conn, $product);

    
    if(mysqli_num_rows($product_query) > 0){
        $rows = mysqli_fetch_assoc($product_query);

        $productname = $rows["productname"];
    }
}

if(isset($_POST['addstocks']))
{
    $date = date("Y-m-d H:i:s");
    $barcode = $_POST['barcode'];
    $selling = $_POST['sellingprice'];
    $units = $_POST['units'];
    $discount = $_POST['discount'];

    $supplier_price_query = "SELECT supplier_price FROM inflow_branch WHERE barcode = '$barcode'";
    $supplier_price_result = mysqli_query($conn, $supplier_price_query);

    $error_message = "";
    if (mysqli_num_rows($supplier_price_result) > 0) {
        $supplier_price_row = mysqli_fetch_assoc($supplier_price_result);
        $supplier_price = $supplier_price_row["supplier_price"];

        if($selling <= $supplier_price){
            $error_message = "Selling price cannot be less than or equal to Supplier price.";
        }else{
            $total_value = $selling * $units;
            $total_value_supplier = $supplier_price * $units;
            $profit =   $selling - ($supplier_price * $units);
            $total_profit =  $profit - $discount;
            $outflow_selling = "INSERT INTO outflow_selling_branch (`date`, `barcode`, `selling_price`, `units_sold`, `discount`, `total_value_selling`, `total_value_supplier`, `profit`, `total_profit`, `code`) VALUES (NOW(), '$barcode', '$selling', '$units' ,'$discount', '$total_value', '$total_value_supplier', '$profit', '$total_profit', '$code')";
            $onflow_query = mysqli_query($conn, $outflow_selling);
        }
    }else{
        $error_message = "Supplier price not found for the specified product.";
    }
}

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href=".../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     -->
    <title>Herb and Angel | Stock Outflow</title>
</head>
<style>
    .btn-container {
            display: flex;
            gap: 10px; /* Adjust the gap between the button and the heading */
        }
</style>

<body>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
               <a href="/branch/branch-dashboard.php" class="logo">
                <img src="../../assets/img/logo (1).png" alt="">
               </a>

               <a href="../branch-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
               </a>

               <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

            <!--Underline Nav-->
            <ul class="nav user-menu">

                <!--Nav Items Dropdown-->
                <li class="nav-item dropdown has-arrow main-drop">
                    <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                        <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                        <span class="status online"></span></span>
                    </a>
                    <div class="dropdown-menu menu-drop-user">
                        <div class="profilename">
                            <div class="profileset">
                                <span class="user-img"><img src="../../assets/img/icons/users1.svg " alt="">
                                <span class="status online"></span></span>
                                <div class="profilesets">
                                <h6><?php echo $fullname?></h6>
                                <h5><?php echo $usertype?></h5>
                                </div>
                            </div>
                            <a class="dropdown-item logout pb-0" href="../../index.php"><img src="../../assets/img/icons/log-out.svg" class="me-2" alt="img">Logout</a>
                        </div>
                    </div>
                </li>
            </ul>

            <div class="dropdown mobile-user-menu">
                <a href="javascript:void(0);" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="../../index.php">Logout</a>
                </div>
            </div>
        </div>

        <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>
                                <li class="active">
                                    <a href="../branch-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                        <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../inventory-management/stockinflow.php">Stock Inflow</a></li>
                                                <li><a href="../inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                                <li><a href="../inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                                <li><a href="../inventory-management/productcost.php">Product Cost</a></li>
                                            </ul>
                                </li>                             

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wrench.svg" alt="img"><span> Mechanic</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../mechanic/mechanic.php">Mechanic</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-file-find.svg" alt="img"><span> Model</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../model/add-model.php">Add Model</a></li>
                                        <li><a href="../model/suggestions.php">Suggestions</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                        <ul>
                                            <li>
                                                <a href="javascript:void(0);"><span style="font-weight:bold;"> Profit</span> <span class="menu-arrow"></span></a>
                                                    <ul>
                                                        <li><a href="../branch/report/product.php">Profit from Products</a></li>
                                                        <li><a href="../branch/report/labor.php">Profit from Labor</a></li>
                                                        <li><a href="../branch/report/profit.php">All Profit</a></li>
                                                    </ul>
                                            </li>
                                            <hr>
                                            <li><a href="../report/inventory.php" style="font-weight:bold;">Inventory Report</a></li>
                                
                                        </ul>
                                </li>
                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> Mechanic Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../mechanic-management/records.php">Accounts</a></li>
                                            </ul>
                                </li>  
                        </ul>
                    </div>
                </div>
            </div>

            <!--Under Main Content-->
            <div class="page-wrapper">
                <div class="content">
                    <div class="page-title">
                        <h4>Stock Outflow</h4>
                        <div class="btn-container">
                        <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#addstocks" onclick="openPopup()">Sold Stocks</button>
                        </div>
                    </div>
                    <table class="table table-striped">
                        <thead class="vertical-headers">
                            <tr>
                                <th scope="col">Date</th>
                                <th scope="col">Product Name</th>
                                <th scope="col">Selling Price</th>
                                <th scope="col">Units Sold</th>
                                <th scope="col">Total value of <br>Selling Price</th>
                                <th scope="col">Supplier Price</th>
                                <th scope="col">Total value of <br>Supplier Price</th>
                                <th scope="col">Profit</th>
                                <th scope="col">Customer <br>Discount</th>
                                <th scope="col">Total <br>Profit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                // Loop through the product results and display them in the table
                                while ($product_row = mysqli_fetch_assoc($product_result)) {
                                    echo '<tr>';
                                    echo '<td>' . $product_row['date'] . '</td>';
                                    echo '<td>' . $product_row['productname'] . '</td>';
                                    echo '<td>' . $product_row['selling_price'] . '</td>';
                                    echo '<td>' . $product_row['units_sold'] . '</td>';
                                    echo '<td>' . $product_row['total_value_selling'] . '</td>';
                                    echo '<td>' . $product_row['supplier_price'] . '</td>';
                                    echo '<td>' . $product_row['total_value_supplier'] . '</td>';
                                    echo '<td>' . $product_row['profit'] . '</td>';
                                    echo '<td>' . $product_row['discount'] . '</td>';
                                    echo '<td>' . $product_row['total_profit'] . '</td>';
                                    echo '</tr>';
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            
    </div>

    <div class="popup" id="addstocks-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="addstocks" tabindex="-1" role="dialog" aria-labelledby="addstocksModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addstocksModalLabel">Add Product</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Barcode</label>
                                                    <input type="text" class="form-control" name="barcode" id="barcode" value="<?php echo @$_POST['barcode']?>" onblur="fetchProductName()" placeholder="Enter Barcode" autofocus>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Product Name</label>
                                                    <input type="text" class="form-control" name="productname" id="productname" value="<?php echo $productname ?>" placeholder="Enter Product Name" readonly>
                                                </div>
                                            </div>
                                            <div class="alert alert-danger" role="alert" id="error-message" style="display:<?php echo empty($error_message) ? 'none' : 'block'; ?>">
                                                <?php echo $error_message; ?>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Selling Price</label>
                                                    <input type="text" class="form-control" name="sellingprice" id="sellingprice" value="<?php echo @$_POST['sellingprice']?>" placeholder="Enter Selling Price" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Units Sold</label>
                                                    <input type="text" class="form-control" name="units" id="units" placeholder="Enter Units Received" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Customer Discount</label>
                                                    <input type="text" class="form-control" name="discount" id="discount" placeholder="Enter Customer Discount">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="addstocks" class="btn btn-submit me-2">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
<script src="../../assets/js/jquery-3.6.0.min.js"></script>

<script src="../../assets/js/feather.min.js"></script>

<script src="../../assets/js/jquery.slimscroll.min.js"></script>

<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>

<script src="../../assets/js/bootstrap.bundle.min.js"></script>

<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>

<script src="../../assets/js/script.js"></script>

<script>
    function openPopup() {
        document.getElementById("addstocks-popup").style.display = "block";
    }
</script>

<script>
    function fetchProductName() {
        var barcode = document.getElementById('barcode').value;
        if (barcode.trim() !== '') {
            $.ajax({
                type: 'POST',
                url: 'fetch_name.php',
                data: { barcode: barcode },
                success: function(response) {
                    document.getElementById('productname').value = response;
                },
                error: function(error) {
                    alert('Error fetching product name from the database.');
                    console.error(error);
                }
            });
        }
    }
</script>

<script>
    function validateSellingPrice() {
        var sellingPrice = parseFloat($('#sellingprice').val());
        var errorMessage = document.getElementById("error-message");

        if (!isNaN(sellingPrice) && !isNaN(<?php echo $supplier_price; ?>) && sellingPrice <= <?php echo $supplier_price; ?>) {
            errorMessage.style.display = "block";
        } else {
            errorMessage.style.display = "none";
        }
    }

    $(document).ready(function() {
        // Trigger form validation when selling price is changed
        $('#sellingprice').on('input', function() {
            validateSellingPrice();
        });

        // Display error message if it exists
        <?php echo empty($error_message) ? '' : 'validateSellingPrice();'; ?>
    });
</script>




</body>
</html>