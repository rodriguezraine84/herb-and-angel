<!DOCTYPE html>
<?php 
include('../../connection.php');
session_start();
if(isset($_SESSION["code"]))
{
    $code = $_SESSION["code"];
}else{
    echo"Record not Found!";
    exit;
}

$name = "SELECT firstname, usertype FROM users_branch WHERE code = $code";
$name_query = mysqli_query($conn, $name);

if(mysqli_num_rows($name_query) > 0){
    $row = mysqli_fetch_assoc($name_query);

    $fullname = $row["firstname"];
    $usertype = $row["usertype"];
}

date_default_timezone_set('Asia/Manila');

if(isset($_POST['add_mechanic'])) {
    $mechanic_id = $_POST['branch'];
    $services = isset($_POST['selected_services']) ? $_POST['selected_services'] : array();
    $service_prices = isset($_POST['service_prices']) ? $_POST['service_prices'] : array();

    if (count($services) > 0 && count($service_prices) > 0) {
        // Loop through selected services and their prices
        for ($i = 0; $i < count($services); $i++) {
            $service_id = $services[$i];
            $price = $service_prices[$i];

            // Insert data into mechanic_services table
            $insert_query = "INSERT INTO mechanic_services (`date`, `mechanic_id`, `services_id`, `price`, `code`) VALUES (NOW(), '$mechanic_id', '$service_id', '$price', '$code')";
            $insert_query_result = mysqli_query($conn, $insert_query);
        }
    } else {
        echo "No services selected! <br>";
    }

    exit; // Exit to prevent further execution
}

$mechanic = "SELECT 
                ms.date,
                ms.mechanic_id,
                mr.mechanic_name,
                SUM(ms.price) AS total_price
            FROM 
                mechanic_services ms
            INNER JOIN 
                mechanic_record mr ON ms.mechanic_id = mr.mechanic_id
            GROUP BY 
                ms.mechanic_id, mr.mechanic_name;";
$mechanic_query = mysqli_query($conn, $mechanic);
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     -->

     <!-- Bootstrap Selectpicker CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.14.0-beta2/css/bootstrap-select.min.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>

    <!-- Bootstrap Selectpicker JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.14.0-beta2/js/bootstrap-select.min.js"></script>

    <title>Herb and Angel | Dashboard</title>
</head>
<style>
.btn-container {
            display: flex;
            gap: 10px; /* Adjust the gap between the button and the heading */
        }
</style>
<body>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
               <a href="../branch-dashboard.php" class="logo">
                <img src="../../assets/img/logo (1).png" alt="">
               </a>

               <a href="../branch-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
               </a>

               <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

            <!--Underline Nav-->
            <ul class="nav user-menu">

            <!--Nav Items Dropdown-->
            <li class="nav-item dropdown has-arrow main-drop">
                <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                    <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                    <span class="status online"></span></span>
                </a>
                <div class="dropdown-menu menu-drop-user">
                    <div class="profilename">
                        <div class="profileset">
                            <span class="user-img"><img src="../../assets/img/icons/users1.svg " alt="">
                            <span class="status online"></span></span>
                            <div class="profilesets">
                            <h6><?php echo $fullname?></h6>
                            <h5><?php echo $usertype?></h5>
                            </div>
                        </div>
                        <a class="dropdown-item logout pb-0" href="../index.php"><img src="../assets/img/icons/log-out.svg" class="me-2" alt="img">Logout</a>
                    </div>
                </div>
            </li>
            </ul>
            </div>

            <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
            <div id="sidebar-menu" class="sidebar-menu">
                <ul>
                        <li class="active">
                            <a href="../branch-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                        </li>

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                            </ul>
                        </li>

                        <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../inventory-management/stockinflow.php">Stock Inflow</a></li>
                                        <li><a href="../inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                        <li><a href="../inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                        <li><a href="../inventory-management/productcost.php">Product Cost</a></li>
                                    </ul>
                        </li>                             

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wrench.svg" alt="img"><span> Mechanic</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../mechanic/mechanic.php">Mechanic</a></li>
                            </ul>
                        </li>

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-file-find.svg" alt="img"><span> Model</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../model/add-model.php">Add Model</a></li>
                                <li><a href="../model/suggestions.php">Suggestions</a></li>
                            </ul>
                        </li>

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                <ul>
                                    <li>
                                        <a href="javascript:void(0);"><span style="font-weight:bold;"> Profit</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../report/product.php">Profit from Products</a></li>
                                                <li><a href="../report/labor.php">Profit from Labor</a></li>
                                                <li><a href="../report/profit.php">All Profit</a></li>
                                            </ul>
                                    </li>
                                    <hr>
                                    <li><a href="../report/inventory.php" style="font-weight:bold;">Inventory Report</a></li>
                        
                                </ul>
                        </li>

                        <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> Mechanic Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../mechanic-management/records.php">Accounts</a></li>
                                    </ul>
                        </li>  
                </ul>
            </div>
            </div>
            </div>
            <!--Under Main Content-->
            <div class="page-wrapper">
                <div class="content">
                    <div class="page-title">
                        <h4>Mechanic Services</h4>
                        <div class="btn-container">
                        <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#addstocks" onclick="openPopup()">Mechanic Services</button>
                        </div>
                    </div>
                    <table class="table table-striped">
                        <thead class="vertical-headers">
                            <tr>
                                <th scope="col">Date</th>
                                <th scope="col">Mechanic Name</th>
                                <th scope="col">Labor Cost</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            while ($emchanic_row = mysqli_fetch_assoc($mechanic_query)) {
                                // Calculate profit as 10% of labor cost or total price (whichever is higher)
                                $labor_cost = $emchanic_row['total_price'];
                                $profit = $labor_cost * 0.1;

                                echo '<tr>';
                                echo '<td>' . $emchanic_row['date'] . '</td>';
                                echo '<td>' . $emchanic_row['mechanic_name'] . '</td>';
                                echo '<td>' . $labor_cost . '</td>';
                                echo '</tr>';
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>

    </div>

    <div class="popup" id="addservice-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="addstocks" tabindex="-1" role="dialog" aria-labelledby="addstocksModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg"role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addstocksModalLabel">Mechanic Service</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                        <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Mechanic Name</label>
                                                    <select id="branch" name="branch" class="form-control" required>
                                                        
                                                    <option value="" selected>Select a Mechanic</option>
                                                    <?php
                                                    $mechanic_name_query = mysqli_query($conn, "SELECT * FROM mechanic_record");

                                                    if(mysqli_num_rows($mechanic_name_query) > 0) {
                                                        while($name_row = mysqli_fetch_assoc($mechanic_name_query)) {
                                                            // Fetch the branch information for each user
                                                            $mechanic_name_code = $name_row["mechanic_id"];
                                                            $mechanic_info_query = mysqli_query($conn, "SELECT * FROM mechanic_record WHERE mechanic_id = '$mechanic_name_code'");
                                                            
                                                            if(mysqli_num_rows($mechanic_info_query) > 0) {
                                                                $mechanic_info = mysqli_fetch_assoc($mechanic_info_query);
                                                                $mechanicName = $mechanic_info["mechanic_name"];
                                                                $mechanicCode = $mechanic_info["mechanic_id"];

                                                                echo "<option value='$mechanic_name_code'>$mechanicName</option>";
                                                            } else {
                                                                echo "<option value=''>No branch found for user!</option>";
                                                            }
                                                        }
                                                    } else {
                                                        echo "<option value=''>No users found!</option>";
                                                    }
                                                    ?>

                                                    </select>
                                                </div>
                                            </div>
                                                    <label>Services</label>
                                                    <div class="container mt-2">
                                                        <select name="selected_services[]" class="selectpicker custom-width form-control" multiple aria-label="Default select example" data-live-search="true" onchange="addPriceField()">

                                                            <?php
                                                            $services_query = mysqli_query($conn, "SELECT * FROM services");

                                                            if(mysqli_num_rows($services_query) > 0) {
                                                                while($service_row = mysqli_fetch_assoc($services_query)) {
                                                                    // Fetch the branch information for each user
                                                                    $service_code = $service_row["service_id"];
                                                                    $service_info_query = mysqli_query($conn, "SELECT * FROM services WHERE service_id = '$service_code'");
                                                                    
                                                                    if(mysqli_num_rows($service_info_query) > 0) {
                                                                        $service_info = mysqli_fetch_assoc($service_info_query);
                                                                        $serviceName = $service_info["service"];
                                                                        $serviceCode = $service_info["service_id"];

                                                                        echo "<option value='$service_code'>$serviceName</option>";
                                                                    } else {
                                                                        echo "<option value=''>No service found!</option>";
                                                                    }
                                                                }
                                                            } else {
                                                                echo "<option value=''>No service found!</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div id="priceFieldContainer" class="mt-1"></div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="add_mechanic" class="btn btn-submit mt-2">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
    function addPriceField() {
        var selectedServices = document.getElementsByName("selected_services[]")[0].selectedOptions;
        var priceFieldContainer = document.getElementById("priceFieldContainer");
        priceFieldContainer.innerHTML = ""; // Clear existing content

        for (var i = 0; i < selectedServices.length; i++) {
            var selectedService = selectedServices[i];
            var inputGroup = document.createElement("div");
            inputGroup.className = "input-group mb-3";
            
            var inputField = document.createElement("input");
            inputField.type = "text";
            inputField.className = "form-control";
            inputField.name = "service_prices[]"; // Ensure it's an array
            inputField.placeholder = "Enter price for " + selectedService.text;

            var breakLine = document.createElement("br");

            var inputGroupText = document.createElement("span");
            inputGroupText.className = "input-group-text";
            inputGroupText.innerHTML = "₱";

            inputGroup.appendChild(inputGroupText);
            inputGroup.appendChild(inputField);
            priceFieldContainer.appendChild(inputGroup);
            priceFieldContainer.appendChild(breakLine);
        }
    }
</script>

    
<script src="../../assets/js/jquery-3.6.0.min.js"></script>

<script src="../../assets/js/feather.min.js"></script>

<script src="../../assets/js/jquery.slimscroll.min.js"></script>

<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>

<script src="../../assets/js/bootstrap.bundle.min.js"></script>

<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>

<script src="../../assets/js/script.js"></script>

<script>
    function openPopup() {
        document.getElementById("addservice-popup").style.display = "block";
    }
</script>


</body>
</html>