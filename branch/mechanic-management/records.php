<!DOCTYPE html>
<?php 
include('../../connection.php');
session_start();
if(isset($_SESSION["code"]))
{
    $code = $_SESSION["code"];
}else{
    echo"Record not Found!";
    exit;
}

$name = "SELECT firstname, usertype FROM users_branch WHERE code = $code";
$name_query = mysqli_query($conn, $name);

if(mysqli_num_rows($name_query) > 0){
    $row = mysqli_fetch_assoc($name_query);

    $fullname = $row["firstname"];
    $usertype = $row["usertype"];
}

if(isset($_POST['add_mechanic'])){
    $mechanic_name = $_POST['mechanic_name'];

    $mechanic = "INSERT INTO mechanic_record (`mechanic_name`, `user_branch_code`) VALUES ('$mechanic_name', '$code')";
    $mechanic_query =  mysqli_query($conn, $mechanic);
}

$mechanic_fetch = "SELECT * FROM mechanic_record WHERE user_branch_code = $code";
$mechanic_fetch_query = mysqli_query($conn, $mechanic_fetch);

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     -->
    <title>Herb and Angel | Dashboard</title>
</head>
<body>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
               <a href="../branch-dashboard.php" class="logo">
                <img src="../../assets/img/logo (1).png" alt="">
               </a>

               <a href="../branch-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
               </a>

               <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

            <!--Underline Nav-->
            <ul class="nav user-menu">

            <!--Nav Items Dropdown-->
            <li class="nav-item dropdown has-arrow main-drop">
                <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                    <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                    <span class="status online"></span></span>
                </a>
                <div class="dropdown-menu menu-drop-user">
                    <div class="profilename">
                        <div class="profileset">
                            <span class="user-img"><img src="../../assets/img/icons/users1.svg " alt="">
                            <span class="status online"></span></span>
                            <div class="profilesets">
                            <h6><?php echo $fullname?></h6>
                            <h5><?php echo $usertype?></h5>
                            </div>
                        </div>
                        <a class="dropdown-item logout pb-0" href="../index.php"><img src="../assets/img/icons/log-out.svg" class="me-2" alt="img">Logout</a>
                    </div>
                </div>
            </li>
            </ul>
            </div>

            <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
            <div id="sidebar-menu" class="sidebar-menu">
                <ul>
                        <li class="active">
                            <a href="../branch-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                        </li>

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                            </ul>
                        </li>

                        <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="./inventory-management/stockinflow.php">Stock Inflow</a></li>
                                        <li><a href="./inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                        <li><a href="./inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                        <li><a href="./inventory-management/productcost.php">Product Cost</a></li>
                                    </ul>
                        </li>                             

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wrench.svg" alt="img"><span> Mechanic</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../mechanic/mechanic.php">Mechanic</a></li>
                            </ul>
                        </li>

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-file-find.svg" alt="img"><span> Model</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../model/add-model.php">Add Model</a></li>
                                <li><a href="../model/suggestions.php">Suggestions</a></li>
                            </ul>
                        </li>

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                <ul>
                                    <li>
                                        <a href="javascript:void(0);"><span style="font-weight:bold;"> Profit</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../report/product.php">Profit from Products</a></li>
                                                <li><a href="../report/labor.php">Profit from Labor</a></li>
                                                <li><a href="../report/profit.php">All Profit</a></li>
                                            </ul>
                                    </li>
                                    <hr>
                                    <li><a href="../report/inventory.php" style="font-weight:bold;">Inventory Report</a></li>
                        
                                </ul>
                        </li>

                        <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> Mechanic Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../mechanic-management/records.php">Accounts</a></li>
                                    </ul>
                        </li>  
                </ul>
            </div>
            </div>
            </div>

            <!--Under Main Content-->
            <div class="page-wrapper">
                <div class="content">
                    <div class="page-title">
                        <h4>Mechanic</h4>
                        <div class="btn-container">
                            <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#addMechanic" onclick="openPopup()">Mechanic Service</button>
                        </div>
                    </div>
                    <table class="table table-striped">
                        <thead class="vertical-headers">
                            <tr>
                                <th scope="col">Mechanic ID</th>
                                <th scope="col">Mechanic Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                while ($product_row = mysqli_fetch_assoc($mechanic_fetch_query)) {
                                    echo '<tr>';
                                    echo '<td>' . $product_row['mechanic_id'] . '</td>';
                                    echo '<td>' . $product_row['mechanic_name'] . '</td>';
                                    echo '<td>';
                                    echo '<input type="hidden" class="form-control" value="' . $product_row["mechanic_id"] . '" name="code" id="code">';
                                    echo '<button type="button" class="btn btn-success editBtn" style="margin-right: 5px;"> <i class="fas fa-edit"></i> </button>';
                                    echo '<button type="button" class="btn btn-danger deleteBtn"><i class="fas fa-trash-alt"></i> </button>';
                                    echo '</td>';
                                    echo '</tr>';
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            

    </div>

    <div class="popup" id="addMechanic-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="addMechanic" tabindex="-1" role="dialog" aria-labelledby="addMechanicModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addstocksModalLabel">Mechanic Accounts</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label>Mechanic Name</label>
                                                    <input type="text" class="form-control" name="mechanic_name" id="mechanic_name" value="" placeholder="Enter Mechanic Name" required>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="add_mechanic" class="btn btn-submit me-2">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--EDIT MODAL-->
<div class="popup" id="editModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="addrecordModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addrecordModalLabel">Update Record</h5>
                            
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="update_record_mechanic.php" method="POST">
                            <div class="modal-body">
                                <input type="hidden" class="form-control" name="edit_code" id="edit_code">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                        <div class="col">
                                                <div class="form-group">
                                                    <label>Mechanic Name</label>
                                                    <input type="text" class="form-control" name="edit_mechanic_name" id="edit_mechanic_name" value="" placeholder="Enter Mechanic Name" required>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="updaterecord" class="btn btn-submit me-2">Update Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <!--DELETE MODAL-->
    <div class="popup" id="deleteModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="deleteModalLabel">Delete Record</h5>
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="delete_record_mechanic.php" method="POST">
                            
                            <div class="modal-body">   
                                <input type="hidden" class="form-control" name="delete_code" id="delete_code">
                                <h4>Are you sure you want to delete this record?</h4>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="recorddelete" class="btn btn-primary">Delete Record</button>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../../assets/js/jquery-3.6.0.min.js"></script>
<script src="../../assets/js/feather.min.js"></script>
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>
<script src="../../assets/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>
<script src="../../assets/js/script.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script>
    function openPopup() {
        document.getElementById("addMechanic-popup").style.display = "block";
    }
</script>

<script>
        $(document).ready(function () {

            $('.editBtn').on('click', function () {

                $('#editModal').modal('show');

                $tr = $(this).closest('tr');

                var data = $tr.children("td").map(function () {
                    return $(this).text();
                }).get();

                console.log(data);

                $('#edit_code').val(data[0]);
                $('#edit_mechanic_name').val(data[1]);
            }); 
        });
</script>

<script>
    $(document).ready(function () {

        $('.deleteBtn').on('click', function () {

            $('#deleteModal').modal('show');

            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function () {
                return $(this).text();
            }).get();

            console.log(data);

            $('#delete_code').val(data[0]);

        });
    });
</script>

</body>
</html>