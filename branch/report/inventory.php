<!DOCTYPE html>
<?php 
include('../../connection.php');
session_start();

if(isset($_SESSION["code"]))
{
    $code = $_SESSION["code"];
}else{
    echo"Record not Found!";
    exit;
}

$name = "SELECT firstname, usertype FROM users_branch WHERE code = $code";
$name_query = mysqli_query($conn, $name);

if(mysqli_num_rows($name_query) > 0){
    $row = mysqli_fetch_assoc($name_query);

    $fullname = $row["firstname"];
    $usertype = $row["usertype"];
}

$product_query = "
            SELECT
            p.productname,
            i.units_received,
            COALESCE(o.units_sold, 0) AS units_sold,
            (i.units_received - COALESCE(o.units_sold, 0)) AS available_stocks,
            o.total_value_selling,  -- Alias added
            o.total_value_supplier  -- Alias added
            FROM
            products p
            JOIN (
            SELECT
                barcode,
                COALESCE(SUM(units_received), 0) AS units_received,
                code
            FROM
                inflow_branch
            GROUP BY
                barcode, code
            ) i ON p.barcode = i.barcode
            LEFT JOIN (
            SELECT
                barcode,
                total_value_selling,
                total_value_supplier,
                COALESCE(SUM(units_sold), 0) AS units_sold
            FROM
                outflow_selling_branch
            GROUP BY
                barcode
            ) o ON p.barcode = o.barcode
            JOIN users_branch ub ON i.code = ub.branch_code
            WHERE ub.code = $code";

$product_result = mysqli_query($conn, $product_query);

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href=".../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     -->
    <title>Herb and Angel | Inventory Analyzer</title>
</head>
<style>
    .blue-highlight {
        background-color: #a9c9ea; /* Adjust the color as needed */
    }

    .red-highlight {
        background-color: #ffaaaa; /* Adjust the color as needed */
    }

    .white-highlight {
        background-color: #FFFFFF; /* Adjust the color as needed */
    }
</style>
<body>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
            <a href="/branch/branch-dashboard.php" class="logo">
                <img src="../../assets/img/logo (1).png" alt="">
            </a>

               <a href="../branch-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
               </a>

               <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

            <!--Underline Nav-->
            <ul class="nav user-menu">

                <!--Nav Items Dropdown-->
                <li class="nav-item dropdown has-arrow main-drop">
                    <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                        <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                        <span class="status online"></span></span>
                    </a>
                    <div class="dropdown-menu menu-drop-user">
                        <div class="profilename">
                            <div class="profileset">
                                <span class="user-img"><img src="../../assets/img/icons/users1.svg " alt="">
                                <span class="status online"></span></span>
                                <div class="profilesets">
                                <h6><?php echo $fullname?></h6>
                                <h5><?php echo $usertype?></h5>
                                </div>
                            </div>
                            <a class="dropdown-item logout pb-0" href="../../index.php"><img src="../../assets/img/icons/log-out.svg" class="me-2" alt="img">Logout</a>
                        </div>
                    </div>
                </li>
            </ul>

            <div class="dropdown mobile-user-menu">
                <a href="javascript:void(0);" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="../../index.php">Logout</a>
                </div>
            </div>
        </div>

        <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>
                                <li class="active">
                                    <a href="../branch-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                        <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../inventory-management/stockinflow.php">Stock Inflow</a></li>
                                                <li><a href="../inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                                <li><a href="../inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                                <li><a href="../inventory-management/productcost.php">Product Cost</a></li>
                                            </ul>
                                </li>                             

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wrench.svg" alt="img"><span> Mechanic</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../mechanic/mechanic.php">Mechanic</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-file-find.svg" alt="img"><span> Model</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../model/add-model.php">Add Model</a></li>
                                        <li><a href="../model/suggestions.php">Suggestions</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                        <ul>
                                            <li>
                                                <a href="javascript:void(0);"><span style="font-weight:bold;"> Profit</span> <span class="menu-arrow"></span></a>
                                                    <ul>
                                                        <li><a href="../branch/report/product.php">Profit from Products</a></li>
                                                        <li><a href="../branch/report/labor.php">Profit from Labor</a></li>
                                                        <li><a href="../branch/report/profit.php">All Profit</a></li>
                                                    </ul>
                                            </li>
                                            <hr>
                                            <li><a href="../report/inventory.php" style="font-weight:bold;">Inventory Report</a></li>
                                
                                        </ul>
                                </li>
                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> Mechanic Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../mechanic-management/records.php">Accounts</a></li>
                                            </ul>
                                </li>  
                        </ul>
                    </div>
                </div>
            </div>

            <!--Under Main Content-->
            <div class="page-wrapper">
                <div class="content">
                    <div class="page-title">
                        <h4>Inventory Report</h4>
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Product Name</th>
                                <th scope="col">Stock Inflow</th>
                                <th scope="col">Stock Outflow</th>
                                <th scope="col">Available Stocks</th>
                                <th scope="col">Total value of <br>Selling Price</th>
                                <th scope="col">Total value of <br>Supplier Price</th>                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            // Initialize variables to hold the grand totals
                            $grand_total_selling = 0;
                            $grand_total_supplier = 0;

                            // Loop through the product results and display them in the table
                            while ($product_row = mysqli_fetch_assoc($product_result)) {
                                echo '<tr';
                                if ($product_row['available_stocks'] == 0) {
                                    echo ' class="red-highlight"';
                                } elseif ($product_row['available_stocks'] >= 1 && $product_row['available_stocks'] <= 2) {
                                    echo ' class="blue-highlight"';
                                } elseif ($product_row['available_stocks'] >= 3) {
                                    echo ' class="white-highlight"';
                                }

                                echo '>';
                                echo '<td>' . $product_row['productname'] . '</td>';
                                echo '<td>' . $product_row['units_received'] . '</td>';
                                echo '<td>' . $product_row['units_sold'] . '</td>';
                                echo '<td>' . $product_row['available_stocks'] . '</td>';
                                echo '<td>' . $product_row['total_value_selling'] . '</td>';
                                echo '<td>' . $product_row['total_value_supplier'] . '</td>';
                                echo '</tr>';

                                // Accumulate total values for selling and supplier
                                $grand_total_selling += $product_row['total_value_selling'];
                                $grand_total_supplier += $product_row['total_value_supplier'];
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Grand Total</th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <?php echo $grand_total_selling; ?>
                                </td>
                                <td>
                                    <?php echo $grand_total_supplier; ?>
                                </td>
                            </tr>
                        </tfoot>

                    </table>
                </div>
            </div>
            
    </div>

    
<script src="../../assets/js/jquery-3.6.0.min.js"></script>

<script src="../../assets/js/feather.min.js"></script>

<script src="../../assets/js/jquery.slimscroll.min.js"></script>

<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>

<script src="../../assets/js/bootstrap.bundle.min.js"></script>

<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>

<script src="../../assets/js/script.js"></script>
    
</body>
</html>