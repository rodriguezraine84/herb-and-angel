<!DOCTYPE html>
<?php 
include('../../connection.php');
session_start();
if(isset($_SESSION["code"]))
{
    $code = $_SESSION["code"];
}else{
    echo"Record not Found!";
    exit;
}

$name = "SELECT firstname, usertype FROM users_branch WHERE code = $code";
$name_query = mysqli_query($conn, $name);

if(mysqli_num_rows($name_query) > 0){
    $row = mysqli_fetch_assoc($name_query);

    $fullname = $row["firstname"];
    $usertype = $row["usertype"];
}
$defaultDate = date('Y-m-d');
$fromDate = isset($_POST['startfrom']) ? $_POST['startfrom'] : $defaultDate;
$toDate = isset($_POST['endto']) ? $_POST['endto'] : $defaultDate;

// If no date is selected, set default date to current date
if ($fromDate == $defaultDate && $toDate == $defaultDate) {
    $fromDate = $toDate = date('Y-m-d');
}

$total_profit_query = "SELECT
                        SUM(CAST(ms.price AS DECIMAL(10, 2))) AS total_profit
                        FROM
                            mechanic_services ms
                        WHERE 
                            ms.code = '$code' AND DATE(ms.date) BETWEEN '$fromDate' AND '$toDate'";
$total_profit_result = mysqli_query($conn,$total_profit_query);

$total_profit_query_item = "SELECT
                        SUM(CAST(osb.total_profit AS DECIMAL(10, 2))) AS total_profit
                        FROM
                            outflow_selling_branch osb
                        WHERE 
                            osb.code = '$code' AND DATE(osb.date) BETWEEN '$fromDate' AND '$toDate'";
$total_profit_query_item_result = mysqli_query($conn, $total_profit_query_item);

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     -->

    <title>Herb and Angel | Branch Profit Report</title>

</head>
<body>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
               <a href="../branch-dashboard.php" class="logo">
                <img src="../../assets/img/logo (1).png" alt="">
                    
               </a>

               <a href="../branch-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
               </a>

               <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

            <!--Underline Nav-->
            <ul class="nav user-menu">

            <!--Nav Items Dropdown-->
            <li class="nav-item dropdown has-arrow main-drop">
                <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                    <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                    <span class="status online"></span></span>
                </a>
                <div class="dropdown-menu menu-drop-user">
                    <div class="profilename">
                        <div class="profileset">
                            <span class="user-img"><img src="../../assets/img/icons/users1.svg " alt="">
                            <span class="status online"></span></span>
                            <div class="profilesets">
                            <h6><?php echo $fullname?></h6>
                            <h5><?php echo $usertype?></h5>
                            </div>
                        </div>
                        <a class="dropdown-item logout pb-0" href="../index.php"><img src="../assets/img/icons/log-out.svg" class="me-2" alt="img">Logout</a>
                    </div>
                </div>
            </li>
            </ul>
            </div>

            <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
            <div id="sidebar-menu" class="sidebar-menu">
                <ul>
                        <li class="active">
                            <a href="../branch-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                        </li>

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                            </ul>
                        </li>

                        <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="./inventory-management/stockinflow.php">Stock Inflow</a></li>
                                        <li><a href="./inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                        <li><a href="./inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                        <li><a href="./inventory-management/productcost.php">Product Cost</a></li>
                                    </ul>
                        </li>                             

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wrench.svg" alt="img"><span> Mechanic</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../mechanic/mechanic.php">Mechanic</a></li>
                            </ul>
                        </li>

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-file-find.svg" alt="img"><span> Model</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../model/add-model.php">Add Model</a></li>
                                <li><a href="../model/suggestions.php">Suggestions</a></li>
                            </ul>
                        </li>

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                <ul>
                                    <li>
                                        <a href="javascript:void(0);"><span style="font-weight:bold;"> Profit</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../report/product.php">Profit from Products</a></li>
                                                <li><a href="../report/labor.php">Profit from Labor</a></li>
                                                <li><a href="../report/profit.php">All Profit</a></li>
                                            </ul>
                                    </li>
                                    <hr>
                                    <li><a href="../report/inventory.php" style="font-weight:bold;">Inventory Report</a></li>
                        
                                </ul>
                        </li>

                        <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> Mechanic Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../mechanic-management/records.php">Accounts</a></li>
                                    </ul>
                        </li>  
                </ul>
            </div>
            </div>
            </div>

            <!--Under Main Content-->
            <div class="page-wrapper">
                <div class="content">
                    <div class="container mt-5">
                        <h3>Select Date</h3>
                        <div style="display: flex; gap: 20px;">
                            <div>
                                <label for="fromDateInput" class="form-label">From:</label>
                                <input type="date" id="fromDateInput" class="form-control" style="width: 100%">
                            </div>
                            <div>
                                <label for="toDateInput" class="form-label">To:</label>
                                <input type="date" id="toDateInput" class="form-control" style="width: 100%">
                            </div>
                        </div>
                    </div>
                </div>
                        <form method="POST" action="">
                            <div style="display: flex; gap: 20px;">
                                <div>
                                    <label for="fromDateInput" class="form-label">From:</label>
                                    <input type="date" id="fromDateInput" name="startfrom" class="form-control" style="width: 100%">
                                </div>
                                <div>
                                    <label for="toDateInput" class="form-label">To:</label>
                                    <input type="date" id="toDateInput" name="endto" class="form-control" style="width: 100%">
                                </div>
                                <button type="submit" name="confirm" id="confirm" class="btn btn-primary mt-5">Confirm</button>
                            </div>
                        </form>
                    </div>
                        <table class="table table-striped" style="overflow-x: auto;">
                            
                            <tbody>
                                <tr>
                                    <td>Profit From Labor</td>
                                    <?php
                                        while ($row = mysqli_fetch_assoc($total_profit_result)) {
                                            $labor_cost = $row['total_profit'];
                                            $profit = $labor_cost * 0.1;
                                            echo '<td>' . $profit . '</td>';
                                        }
                                    ?>
                                </tr>

                                <tr>
                                    <td>Profit From Item
                                        
                                    </td>
                                        <?php
                                        while ($rows = mysqli_fetch_assoc($total_profit_query_item_result)) {
                                            $item_cost = $rows['total_profit'];
                                            echo '<td>' . $item_cost . '</td>';
                                        }
                                        ?>
                                </tr>
                                <tr>
                                    <td>Gross Profit</td>
                                    <?php
                                    if (isset($profit) && isset($item_cost)) {
                                        $gross_profit = $profit + $item_cost;
                                        echo "<td>" . number_format($gross_profit, 2) . "</td>";
                                    } else {
                                        echo "<td>No data available</td>";
                                    }
                                    ?>
                                </tr>
                            </tbody>
                        </table>
                        
                </div>

                
>>>>>>> 541ce5361965c0d57c7401c73cb479d1197b04d5
            </div>
    </div>

    
<script src="../../assets/js/jquery-3.6.0.min.js"></script>

<script src="../../assets/js/feather.min.js"></script>

<script src="../../assets/js/jquery.slimscroll.min.js"></script>

<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>

<script src="../../assets/js/bootstrap.bundle.min.js"></script>

<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>

<script src="../../assets/js/script.js"></script>
    
</body>
</html>