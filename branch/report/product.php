<!DOCTYPE html>
<?php 
include('../../connection.php');
session_start();
if(isset($_SESSION["code"]))
{
    $code = $_SESSION["code"];
}else{
    echo"Record not Found!";
    exit;
}

$name = "SELECT firstname, usertype FROM users_branch WHERE code = $code";
$name_query = mysqli_query($conn, $name);

if(mysqli_num_rows($name_query) > 0){
    $row = mysqli_fetch_assoc($name_query);

    $fullname = $row["firstname"];
    $usertype = $row["usertype"];
}

$defaultDate = date('Y-m-d');
$fromDate = isset($_POST['startfrom']) ? $_POST['startfrom'] : $defaultDate;
$toDate = isset($_POST['endto']) ? $_POST['endto'] : $defaultDate;

// If no date is selected, set default date to current date
if ($fromDate == $defaultDate && $toDate == $defaultDate) {
    $fromDate = $toDate = date('Y-m-d');
}

// Construct the SQL query based on selected or default dates
$sql = "SELECT DISTINCT
        o.date,
        p.productname,
        o.selling_price,
        o.units_sold,
        i.supplier_price,
        i.totalvalue,
        o.total_value_selling,
        o.total_value_supplier,
        o.profit,
        o.discount,
        o.total_profit
        FROM
        outflow_selling_branch o
        JOIN
        inflow_branch i ON o.barcode = i.barcode
        JOIN
        products p ON o.barcode = p.barcode
        WHERE o.code = $code AND DATE(o.date) BETWEEN '$fromDate' AND '$toDate'";

$query = mysqli_query($conn, $sql);


?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     -->
    <title>Herb and Angel | Branch Product Report</title>
</head>
<body>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
               <a href="../branch-dashboard.php" class="logo">
                <img src="../../assets/img/logo (1).png" alt="">
                    
               </a>

               <a href="../branch-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
               </a>

               <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

            <!--Underline Nav-->
            <ul class="nav user-menu">

            <!--Nav Items Dropdown-->
            <li class="nav-item dropdown has-arrow main-drop">
                <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                    <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                    <span class="status online"></span></span>
                </a>
                <div class="dropdown-menu menu-drop-user">
                    <div class="profilename">
                        <div class="profileset">
                            <span class="user-img"><img src="../../assets/img/icons/users1.svg " alt="">
                            <span class="status online"></span></span>
                            <div class="profilesets">
                            <h6><?php echo $fullname?></h6>
                            <h5><?php echo $usertype?></h5>
                            </div>
                        </div>
                        <a class="dropdown-item logout pb-0" href="../index.php"><img src="../assets/img/icons/log-out.svg" class="me-2" alt="img">Logout</a>
                    </div>
                </div>
            </li>
            </ul>
            </div>

            <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
            <div id="sidebar-menu" class="sidebar-menu">
                <ul>
                        <li class="active">
                            <a href="../branch-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                        </li>

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                            </ul>
                        </li>

                        <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="./inventory-management/stockinflow.php">Stock Inflow</a></li>
                                        <li><a href="./inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                        <li><a href="./inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                        <li><a href="./inventory-management/productcost.php">Product Cost</a></li>
                                    </ul>
                        </li>                             

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-wrench.svg" alt="img"><span> Mechanic</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../mechanic/mechanic.php">Mechanic</a></li>
                            </ul>
                        </li>

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-file-find.svg" alt="img"><span> Model</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="../model/add-model.php">Add Model</a></li>
                                <li><a href="../model/suggestions.php">Suggestions</a></li>
                            </ul>
                        </li>

                        <li class="submenu">
                            <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                <ul>
                                    <li>
                                        <a href="javascript:void(0);"><span style="font-weight:bold;"> Profit</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../report/product.php">Profit from Products</a></li>
                                                <li><a href="../report/labor.php">Profit from Labor</a></li>
                                                <li><a href="../report/profit.php">All Profit</a></li>
                                            </ul>
                                    </li>
                                    <hr>
                                    <li><a href="../report/inventory.php" style="font-weight:bold;">Inventory Report</a></li>
                        
                                </ul>
                        </li>

                        <li class="submenu">
                                <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> Mechanic Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../mechanic-management/records.php">Accounts</a></li>
                                    </ul>
                        </li>  
                </ul>
            </div>
            </div>
            </div>

            <!--Under Main Content-->
            <div class="page-wrapper">
                <div class="content">
                    <div class="container mt-5">
                        <h3>Select Date</h3>
                        <form method="POST" action="">
                            <div style="display: flex; gap: 20px;">
                                <div>
                                    <label for="fromDateInput" class="form-label">From:</label>
                                    <input type="date" id="fromDateInput" name="startfrom" class="form-control" style="width: 100%">
                                </div>
                                <div>
                                    <label for="toDateInput" class="form-label">To:</label>
                                    <input type="date" id="toDateInput" name="endto" class="form-control" style="width: 100%">
                                </div>
                                <button type="submit" name="confirm" id="confirm" class="btn btn-primary mt-5">Confirm</button>
                            </div>
                        </form>
                    </div>
                        <table class="table table-striped" style="overflow-x: auto;">
                            <thead>
                                <tr>
                                    <th scope="col">Date</th>
                                    <th scope="col">Product Name</th>
                                    <th scope="col">Selling Price</th>
                                    <th scope="col">Units Sold</th>
                                    <th scope="col">Total value of <br>Selling Price</th>
                                    <th scope="col">Supplier Price</th>
                                    <th scope="col">Total Value of <br>Supplier Price</th>
                                    <th scope="col">Profit</th>
                                    <th scope="col">Discount</th>
                                    <th scope="col">Total Profit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    // Loop through the product results and display them in the table
                                    while ($product_rows = mysqli_fetch_assoc($query)) {
                                        echo '<tr>';
                                        echo '<td>' . $product_rows['date'] . '</td>';
                                        echo '<td>' . $product_rows['productname'] . '</td>';
                                        echo '<td>' . $product_rows['selling_price'] . '</td>';
                                        echo '<td>' . $product_rows['units_sold'] . '</td>';
                                        echo '<td>' . $product_rows['total_value_selling'] . '</td>';
                                        echo '<td>' . $product_rows['supplier_price'] . '</td>';
                                        echo '<td>' . $product_rows['total_value_supplier'] . '</td>';
                                        echo '<td>' . $product_rows['profit'] . '</td>';
                                        echo '<td>' . $product_rows['discount'] . '</td>';
                                        echo '<td>' . $product_rows['total_profit'] . '</td>';
                                        echo '</tr>';
                                    }
                                ?>
                            </tbody>
                        </table>
                </div>

                
            </div>
    </div>

    
<script src="../../assets/js/jquery-3.6.0.min.js"></script>

<script src="../../assets/js/feather.min.js"></script>

<script src="../../assets/js/jquery.slimscroll.min.js"></script>

<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>

<script src="../../assets/js/bootstrap.bundle.min.js"></script>

<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>

<script src="../../assets/js/script.js"></script>
    
</body>
</html>