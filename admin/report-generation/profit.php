<!DOCTYPE html>
<?php 
include('../../connection.php');
session_start();

if(isset($_SESSION["code"]))
{
    $code = $_SESSION["code"];
}else{
    echo "Record not Found!";
    exit;
}

$name = "SELECT firstname, usertype FROM users WHERE code = $code";
$name_query = mysqli_query($conn, $name);

if(mysqli_num_rows($name_query) > 0){
    $row = mysqli_fetch_assoc($name_query);

    $fullname = $row["firstname"];
    $usertype = $row["usertype"];
}

if(isset($_POST['addrecord'])){
    $code = $_POST['code'];
    $supplier_name = $_POST['suppliername'];
    $supplier_address = $_POST['address'];
    $contact_person = $_POST['person'];
    $contact_number = $_POST['number'];
    $email = $_POST['email'];

    $supplier_record = "INSERT INTO supplier_record (`code`, `supplier_name`, `supplier_address`, `contact_person`, `contact_number`, `email`) 
    VALUES ('$code','$supplier_name','$supplier_address','$contact_person','$contact_number','$email')";
    $supplier_record_query = mysqli_query($conn, $supplier_record);
}

$supplier_users = "SELECT * FROM supplier_record";
$supplier_result = mysqli_query($conn, $supplier_users);

$defaultDate = date('Y-m-d');
$fromDate = isset($_POST['startfrom']) ? $_POST['startfrom'] : $defaultDate;
$toDate = isset($_POST['endto']) ? $_POST['endto'] : $defaultDate;

// Conditionally adjust the SQL query
if ($fromDate == $defaultDate && $toDate == $defaultDate) {
    // Default date, retrieve data for the current date
    $sql = "SELECT * FROM outflow_selling_branch WHERE date = '$defaultDate'";
} else {
    // Date range is selected, retrieve data for the selected date range
    $sql = "SELECT * FROM outflow_selling_branch WHERE date BETWEEN '$fromDate' AND '$toDate'";
}

$total_orders = "SELECT SUM(total_profit) AS total_sales
FROM outflow_selling_branch WHERE date BETWEEN '$fromDate' AND '$toDate'";
$orders_query = mysqli_query($conn, $total_orders);

if ($orders_query) {
    $row = mysqli_fetch_assoc($orders_query);
    $total_orders = $row['total_sales'];    
    $formatted_total_orders = number_format($total_orders, 2);
}
$query = mysqli_query($conn, $sql);

$selected_branches = isset($_POST['selected_branches']) ? $_POST['selected_branches'] : array();
if (isset($_POST['generate_report']) && isset($_POST['selected_branches'])) {
    $selected_branches = $_POST['selected_branches'];
    
    $branch_conditions = implode("','", $selected_branches);
    $branch_conditions = "('$branch_conditions')";

    $sql = "SELECT * FROM outflow_selling WHERE dates BETWEEN '$fromDate' AND '$toDate' AND branch_code IN $branch_conditions";
}

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <script src="https://cdn.rawgit.com/serratus/quaggaJS/0.12.1/dist/quagga.min.js"></script>

    <!-- Bootstrap Selectpicker CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.14.0-beta2/css/bootstrap-select.min.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>

    <!-- Bootstrap Selectpicker JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.14.0-beta2/js/bootstrap-select.min.js"></script>

    
    
    <title>Herb and Angel | Report Generation Profit</title>
</head>
<style>
        .page-title {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .page-title h4 {
            margin: 0;
        }

        .btn-container {
            display: flex;
            gap: 10px; /* Adjust the gap between the button and the heading */
        }

        .custom-width {
            width: 100% !important;
        }
    </style>
<body>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
                <a href="./super-admin-dashboard.php" class="logo">
                    <img src="../../assets/img/logo (1).png" alt="">
                </a>

                <a href="super-admin-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
                </a>

                <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

            <!--Underline Nav-->
            <ul class="nav user-menu">

                <!--Nav Items Dropdown-->
                <li class="nav-item dropdown has-arrow main-drop">
                    <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                        <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                        <span class="status online"></span></span>
                    </a>
                    <div class="dropdown-menu menu-drop-user">
                        <div class="profilename">
                            <div class="profileset">
                                <span class="user-img"><img src="../../assets/img/icons/users1.svg " alt="">
                                <span class="status online"></span></span>
                                <div class="profilesets">
                                <h6><?php echo $fullname?></h6>
                                <h5><?php echo $usertype?></h5>
                                </div>
                            </div>
                            <a class="dropdown-item logout pb-0" href="../../index.php"><img src="../../assets/img/icons/log-out.svg" class="me-2" alt="img">Logout</a>
                        </div>
                    </div>
                </li>
            </ul>

            <div class="dropdown mobile-user-menu">
                <a href="javascript:void(0);" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="../../index.php">Logout</a>
                </div>
            </div>
        </div>

        <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>
                                <li class="active">
                                    <a href="../super-admin-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-store-alt.svg" alt="img"><span> Dealer Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../dealer-management/records.php">Dealer Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-package.svg" alt="img"><span> Supplier Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../supplier-management/records.php">Supplier Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-git-branch.svg" alt="img"><span> Branch Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../branch-management/records.php">Branch Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-cog.svg" alt="img"><span> Services Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../services-management/services.php">Services</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../..//assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../product-management/product-list.php">Add Product</a></li>
                                        <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                        <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../inventory-management/stockinflow.php">Stock Inflow</a></li>
                                                <li><a href="../inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                                <li><a href="../inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                                <li><a href="../inventory-management/productcost.php">Product Cost</a></li>
                                            </ul>
                                </li>
                                
                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> User Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../user-management/accounts.php">Accounts</a></li>
                                            </ul>
                                </li>   

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../report-generation/profit.php">Profit</a></li>
                                        <li><a href="../report-generation/mechanic.php">Mechanic</a></li>
                                
                                    </ul>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>

        <!--Under Main Content-->
        <div class="page-wrapper">
            <div class="content">
                <div class="page-title">
                    <h2>Report Generation</h2>
                </div>
                <div class="container">
                    <form method="POST" action="">

                        <div class="row">
                            <div class="col-md-6">
                                <form>
                                    <div class="form-group">
                                        <label for="startfrom">Start From:</label>
                                        <input type="date" class="form-control" id="startfrom" name="startfrom">
                                    </div>
                                    
                                </form>
                            </div>
                            <div class="col-md-6">
                                <form>
                                    <div class="form-group">
                                        <label for="endto">End To:</label>
                                        <input type="date" class="form-control" id="endto" name="endto">
                                    </div>
                                </form>
                            </div>

                            <!-- Search Bar -->
                            <h3>SELECT BRANCHES: </h3>
                            <div class="container mt-2">
                                <select name="selected_branches[]"class="selectpicker custom-width" multiple aria-label="Default select example" data-live-search="true">
                                
                                    <?php
                                    $users_branch_query = mysqli_query($conn, "SELECT * FROM users_branch");

                                    if(mysqli_num_rows($users_branch_query) > 0) {
                                        while($user_row = mysqli_fetch_assoc($users_branch_query)) {
                                            // Fetch the branch information for each user
                                            $user_branch_code = $user_row["branch_code"];
                                            $branch_info_query = mysqli_query($conn, "SELECT * FROM branch_record WHERE code = '$user_branch_code'");
                                            
                                            if(mysqli_num_rows($branch_info_query) > 0) {
                                                $branch_info = mysqli_fetch_assoc($branch_info_query);
                                                $branchName = $branch_info["branch_name"];
                                                $branchCode = $branch_info["code"];

                                                echo "<option value='$user_branch_code'>$branchName</option>";
                                            } else {
                                                echo "<option value=''>No branch found for user!</option>";
                                            }
                                        }
                                    } else {
                                        echo "<option value=''>No users found!</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <button type="submit" name="generate_report" id="generate_report" class="btn btn-primary mt-5">Generate Report</button>
                        </div>
                    </form>
                    <div class="row">
                    <table class="table table-striped mt-3">
                            <thead>
                                <tr>
                                    <th scope="col">Product From</th>
                                    <?php
                                        if (isset($selected_branches) && is_array($selected_branches)) {
                                            foreach ($selected_branches as $selected_branch_code) {
                                                $branch_info_query = mysqli_query($conn, "SELECT * FROM branch_record WHERE code = '$selected_branch_code'");
                                                if (mysqli_num_rows($branch_info_query) > 0) {
                                                    $branch_info = mysqli_fetch_assoc($branch_info_query);
                                                    $branchName = $branch_info["branch_name"];
                                                    echo "<th scope='col'>$branchName</th>";
                                                }
                                            }
                                        } else {
                                            // Handle the case when $selected_branches is not set or is not an array
                                            // For example, you can display a message or take appropriate action.
                                            echo "<th scope='col'>No branches selected</th>";
                                        }

                                    ?>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Profit From Labor</td>
                                    <?php
                                    if (isset($selected_branches) && is_array($selected_branches)) {
                                        $total_profit_labor = 0; // Initialize total labor profit outside of loop
                                        foreach ($selected_branches as $selected_branch_code) {
                                            // Assuming the foreign key relationship is users -> branch_record -> outflow_selling_branch
                                            $total_profit_query = mysqli_query($conn, "SELECT
                                                SUM(CAST(ms.price AS DECIMAL(10, 2))) AS total_profit
                                                FROM
                                                    mechanic_services ms
                                                JOIN users u ON ms.code = u.code
                                                JOIN users_branch ub ON u.code = ub.code
                                                WHERE 
                                                    ub.branch_code = '$selected_branch_code'");
                                            
                                            if (mysqli_num_rows($total_profit_query) > 0) {
                                                $total_profit_result = mysqli_fetch_assoc($total_profit_query);
                                                $total_profit = $total_profit_result["total_profit"];
                                                
                                                if ($total_profit >= 500) {
                                                    $total_profit_labor += $total_profit * 0.1; // Add labor profit if total profit exceeds $500
                                                }
                                            }
                                        }
                                        // Display labor profit after the loop
                                        echo "<td>" . number_format($total_profit_labor, 2) . "</td>";
                                    } else {
                                        echo "<td>No branches selected</td>";
                                    }
                                    ?>
                                </tr>

                                <tr>
                                    <td>Profit From Item</td>
                                    <?php
                                    if (isset($selected_branches) && is_array($selected_branches)) {
                                        $total_profit_item = 0; // Initialize total item profit outside of loop
                                        foreach ($selected_branches as $selected_branch_code) {
                                            // Assuming the foreign key relationship is users -> branch_record -> outflow_selling_branch
                                            $total_profit_query = mysqli_query($conn, "SELECT
                                                SUM(CAST(osb.total_profit AS DECIMAL(10, 2))) AS total_profit
                                                FROM
                                                    outflow_selling_branch osb
                                                JOIN users u ON osb.code = u.code
                                                JOIN users_branch ub ON u.code = ub.code
                                                WHERE 
                                                    ub.branch_code = '$selected_branch_code'");
                                            if (mysqli_num_rows($total_profit_query) > 0) {
                                                $total_profit_result = mysqli_fetch_assoc($total_profit_query);
                                                $total_profit_item += $total_profit_result["total_profit"];
                                            }
                                        }
                                        // Display item profit after the loop
                                        echo "<td>" . number_format($total_profit_item, 2) . "</td>";
                                    } else {
                                        echo "<td>No branches selected</td>";
                                    }
                                    ?>
                                </tr>
                                <tr>
                                    <td>Gross Profit</td>
                                    <?php
                                    if (isset($total_profit_labor) && isset($total_profit_item)) {
                                        $gross_profit = $total_profit_labor + $total_profit_item;
                                        echo "<td>" . number_format($gross_profit, 2) . "</td>";
                                    } else {
                                        echo "<td>No data available</td>";
                                    }
                                    ?>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

<script src="../../assets/js/jquery-3.6.0.min.js"></script>
<script src="../../assets/js/feather.min.js"></script>
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>
<script src="../../assets/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>
<script src="../../assets/js/script.js"></script>

</body>
</html>