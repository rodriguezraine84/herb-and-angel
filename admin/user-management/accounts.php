<!DOCTYPE html>
<?php 
include('../../connection.php');
session_start();

if(isset($_SESSION["code"]))
{
    $code = $_SESSION["code"];
}else{
    echo"Record not Found!";
    exit;
}

$name = "SELECT firstname, usertype FROM users WHERE code = $code";
$name_query = mysqli_query($conn, $name);

if(mysqli_num_rows($name_query) > 0){
    $row = mysqli_fetch_assoc($name_query);

    $fullname = $row["firstname"];
    $usertype = $row["usertype"];
}

if(isset($_POST['addrecord'])){
    $full_name = $_POST['fullname'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $contact = $_POST['contact'];
    $usertype = $_POST['usertype'];
    $branch = $_POST['branch'];

    $insert_user = "INSERT INTO users_branch (`firstname`, `email`, `password`, `contact`, `usertype`, `branch_code`) VALUES ('$full_name',
                    '$email', '$password', '$contact', '$usertype', '$branch')";
    $insert_user_query = mysqli_query($conn, $insert_user);
}

$users = "SELECT * FROM users_branch";
$users_result = mysqli_query($conn, $users);

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <script src="https://cdn.rawgit.com/serratus/quaggaJS/0.12.1/dist/quagga.min.js"></script>
    <title>Herb and Angel | User Management</title>
</head>
<style>
        .page-title {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .page-title h4 {
            margin: 0;
        }

        .btn-container {
            display: flex;
            gap: 10px; /* Adjust the gap between the button and the heading */
        }
    </style>
<body>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
                <a href="./super-admin-dashboard.php" class="logo">
                    <img src="../../assets/img/logo (1).png" alt="">
                </a>

                <a href="super-admin-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
                </a>

                <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>
            

            <!--Underline Nav-->
            <ul class="nav user-menu">

                <!--Nav Items Dropdown-->
                <li class="nav-item dropdown has-arrow main-drop">
                    <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                        <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                        <span class="status online"></span></span>
                    </a>
                    <div class="dropdown-menu menu-drop-user">
                        <div class="profilename">
                            <div class="profileset">
                                <span class="user-img"><img src="../../assets/img/icons/users1.svg " alt="">
                                <span class="status online"></span></span>
                                <div class="profilesets">
                                <h6><?php echo $fullname?></h6>
                                <h5><?php echo $usertype?></h5>
                                </div>
                            </div>
                            <a class="dropdown-item logout pb-0" href="../../index.php"><img src="../../assets/img/icons/log-out.svg" class="me-2" alt="img">Logout</a>
                        </div>
                    </div>
                </li>
            </ul>

            <div class="dropdown mobile-user-menu">
                <a href="javascript:void(0);" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="../../index.php">Logout</a>
                </div>
            </div>
        </div>

            <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>
                                <li class="active">
                                    <a href="../super-admin-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-store-alt.svg" alt="img"><span> Dealer Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../dealer-management/records.php">Dealer Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-package.svg" alt="img"><span> Supplier Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../supplier-management/records.php">Supplier Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-git-branch.svg" alt="img"><span> Branch Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../branch-management/records.php">Branch Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-cog.svg" alt="img"><span> Services Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../services-management/services.php">Services</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../..//assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../product-management/product-list.php">Add Product</a></li>
                                        <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                        <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../inventory-management/stockinflow.php">Stock Inflow</a></li>
                                                <li><a href="../inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                                <li><a href="../inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                                <li><a href="../inventory-management/productcost.php">Product Cost</a></li>
                                            </ul>
                                </li>
                                
                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> User Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../user-management/accounts.php">Accounts</a></li>
                                            </ul>
                                </li>   

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../report-generation/profit.php">Profit</a></li>
                                        <li><a href="../report-generation/mechanic.php">Mechanic</a></li>
                                
                                    </ul>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>

        <!--Under Main Content-->
        <div class="page-wrapper">
            <div class="content">
                <div class="page-title">
                    <h4>Accounts</h4>
                    <div class="btn-container">
                        <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#addrecord" onclick="openPopup()">Add Record</button>
                    </div>
                </div>
                
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">User ID</th>
                            <th scope="col">Full Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Contact Number</th>
                            <th scope="col">Usertype</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        while ($product_row = mysqli_fetch_assoc($users_result)) {
                            echo '<tr>';
                            echo '<td>' . $product_row['code'] . '</td>';
                            echo '<td>' . $product_row['firstname'] . '</td>';
                            echo '<td>' . $product_row['email'] . '</td>';
                            echo '<td>' . $product_row['contact'] . '</td>';
                            echo '<td>' . $product_row['usertype'] . '</td>';
                            echo '<td>';
                            echo '<input type="hidden" class="form-control" value="' . $product_row["code"] . '" name="code" id="code">';
                            echo '<button type="button" class="btn btn-success editBtn" style="margin-right: 5px;"> <i class="fas fa-edit"></i> </button>';
                            echo '<button type="button" class="btn btn-danger deleteBtn"><i class="fas fa-trash-alt"></i> </button>';
                            echo '</td>';
                            echo '</tr>';
                        }
                    
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="popup" id="addrecord-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="addrecord" tabindex="-1" role="dialog" aria-labelledby="addrecordModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addrecordModalLabel">Add Accounts</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" class="form-control" name="fullname" id="fullname" value="" placeholder="Enter Full Name" autofocus>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="email" id="email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="text" class="form-control" name="password" id="password" placeholder="Enter Password" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control" name="contact" id="contact" placeholder="Enter Contact" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label for="usertype">Usertype</label>
                                                    <select id="usertype" name="usertype" class="form-control" required>
                                                        <option value="">Select Usertype</option>
                                                        <option value="Branch Manager">Branch Manager</option>
                                                        <option value="Provincial Branch Manager">Provincial Branch Manager</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Branch</label>
                                                    <select id="branch" name="branch" class="form-control" required>
                                                    <option value="" selected>Select a Branch</option>
                                                    <?php
                                                        $branch_name = "SELECT * FROM branch_record";
                                                        $branch_name_query = mysqli_query($conn, $branch_name);

                                                        if(mysqli_num_rows($branch_name_query) > 0) {
                                                            while($row = mysqli_fetch_assoc($branch_name_query)) {
                                                                $branchName = $row["branch_address"];
                                                                $branchCode = $row["code"];

                                                                echo "<option value='$branchCode'>$branchName</option>";
                                                            }
                                                        } else {
                                                            echo "<option value=''>No branches found!</option>";
                                                        }
                                                    ?>
                                                    </select>
                                                </div> 
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="addrecord" class="btn btn-submit me-2">Add Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--EDIT MODAL-->
    <div class="popup" id="editModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="addrecordModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addrecordModalLabel">Update Record</h5>
                            
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="update_record_user.php" method="POST">
                            <div class="modal-body">
                                <input type="hidden" class="form-control" name="edit_code" id="edit_code">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input type="text" class="form-control" name="edit_fullname" id="edit_fullname" value="" placeholder="Enter Full Name">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name="edit_email" id="edit_email" placeholder="Enter Email" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control" name="edit_number" id="edit_number" placeholder="Enter Contact Number" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label for="edit_usertype">Usertype</label>
                                                    <select id="edit_usertype" name="edit_usertype" class="form-control" required>
                                                        <option value="">Select Usertype</option>
                                                        <option value="Branch Manager">Branch Manager</option>
                                                        <option value="Provincial Branch Manager">Provincial Branch Manager</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="updaterecord" class="btn btn-submit me-2">Update Record</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <!--DELETE MODAL-->
    <div class="popup" id="deleteModal-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="deleteModalLabel">Delete Record</h5>
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">X</button>
                        </div>
                        <form action="delete_record_user.php" method="POST">
                            
                            <div class="modal-body">   
                                <input type="hidden" class="form-control" name="delete_code" id="delete_code">
                                <h4>Are you sure you want to delete this record?</h4>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="recorddelete" class="btn btn-primary">Delete Record</button>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

<script src="../../assets/js/jquery-3.6.0.min.js"></script>
<script src="../../assets/js/feather.min.js"></script>
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>
<script src="../../assets/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>
<script src="../../assets/js/script.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script>
    function openPopup() {
        document.getElementById("addrecord-popup").style.display = "block";
    }
</script>

<script>
    function updateUserType(selectedType) {
        document.getElementById('usertypeBtn').innerHTML = selectedType;
    }
</script>

<script>
        $(document).ready(function () {

            $('.editBtn').on('click', function () {

                $('#editModal').modal('show');

                $tr = $(this).closest('tr');

                var data = $tr.children("td").map(function () {
                    return $(this).text();
                }).get();

                console.log(data);

                $('#edit_code').val(data[0]);
                $('#edit_fullname').val(data[1]);
                $('#edit_email').val(data[2]);
                $('#edit_number').val(data[3]);
                $('#edit_usertype').val(data[4]);
            }); 
        });
</script>

<script>
    $(document).ready(function () {

        $('.deleteBtn').on('click', function () {

            $('#deleteModal').modal('show');

            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function () {
                return $(this).text();
            }).get();

            console.log(data);

            $('#delete_code').val(data[0]);

        });
    });
</script>
</body>
</html>