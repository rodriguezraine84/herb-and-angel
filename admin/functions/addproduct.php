<?php
session_start();
include('../../connection.php');

if (isset($_POST['addproduct'])) {
    $barcode = $_POST['barcode'];
    $product_name = $_POST['productname'];
    $description = $_POST['$description'];
    $model = $_POST['model'];
    // Check if barcode or product name already exists
    $check_duplicate = "SELECT * FROM products WHERE barcode = '$barcode' OR productname = '$product_name'";
    $duplicate_query = mysqli_query($conn, $check_duplicate);

    if (mysqli_num_rows($duplicate_query) > 0) {
        // Duplicate found
        $_SESSION['message'] = "Error: Product with name '$product_name' already exists.";
        $_SESSION['message_type'] = "danger";
        header('Location: ../product-management/product-list.php');
        exit(0);
    }

    // Insert new product if no duplicate is found
    $product_add = "INSERT INTO products (barcode, productname, description, model) VALUES ('$barcode', '$product_name', '$description', '$model')";
    $product_add_query = mysqli_query($conn, $product_add);

    if ($product_add_query) {
        $_SESSION['message'] = "Successfully Added Product";
        $_SESSION['message_type'] = "success";
        header('Location: ../product-management/product-list.php');
        exit(0);
    } else {
        $_SESSION['message'] = "Failed to Add Product";
        $_SESSION['message_type'] = "danger";
        header('Location: ../product-management/product-list.php');
        exit(0);
    }
}
?>

