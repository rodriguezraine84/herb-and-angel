<?php 
session_start();
include('../../connection.php');

require '../../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if(isset($_POST['save_excel_data']))
{
    $fileName = $_FILES['import_file']['name'];
    $file_ext = pathinfo($fileName, PATHINFO_EXTENSION);

    $allowed_ext = ['xls','csv','xlsx'];

    if(in_array($file_ext, $allowed_ext))
    {
        $inputFileNamePath = $_FILES['import_file']['tmp_name'];
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileNamePath);
        $data = $spreadsheet->getActiveSheet()->toArray();

        $count = 0;
        foreach($data as $row)
        {
            if($count > 0)
            {
                $name = $row['1'];
                $barcode = $row['0']; // Assuming barcode is in column 2

                // Check if the product with the same name or barcode already exists
                $checkQuery = "SELECT COUNT(*) as count FROM products WHERE productname = '$name' OR barcode = '$barcode'";
                $checkResult = mysqli_query($conn, $checkQuery);
                $checkData = mysqli_fetch_assoc($checkResult);

                if($checkData['count'] == 0)
                {
                    $productQuery = "INSERT INTO products (productname, barcode) VALUES ('$name', '$barcode')";
                    $result = mysqli_query($conn, $productQuery);
                    $msg = true;
                }
                else
                {
                    $_SESSION['message'] = "Error: Product with name '$name' already exists.";
                    $_SESSION['message_type'] = "danger";
                    header('Location: ../product-management/product-list.php');
                    exit(0);
                }
            }
            else
            {
                $count = 1;
            }
        }

        if(isset($msg))
        {
            $_SESSION['message'] = "Successfully Imported";
            $_SESSION['message_type'] = "success";
            header('Location: ../product-management/product-list.php');
            exit(0);
        }
    }
    else
    {
        $_SESSION['message'] = "Invalid File";
        $_SESSION['message_type'] = "danger";
        header('Location: ../product-management/product-list.php');
        exit(0);
    }
}
?>
