<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Barcode Scanner</title>
    <!-- Include jQuery -->
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</head>
<body>

    <label for="barcode">Scan Barcode:</label>
    <input type="text" id="barcode" name="barcode" onblur="fetchProductName()" required>
    
    <label for="productName">Product Name:</label>
    <input type="text" id="productName" name="productName" readonly>

    <script>
        function fetchProductName() {
            var barcode = document.getElementById('barcode').value;

            // Check if barcode is not empty
            if (barcode.trim() !== '') {
                // Use AJAX to send the barcode to the PHP script for database query
                $.ajax({
                    type: 'POST',
                    url: '../functions/fetch-name.php', // Adjust this to your PHP script
                    data: { barcode: barcode },
                    success: function(response) {
                        // Display the product name in the input field
                        document.getElementById('productName').value = response;
                    },
                    error: function(error) {
                        alert('Error fetching product name from the database.');
                        console.error(error);
                    }
                });
            }
        }
    </script>

</body>
</html>
