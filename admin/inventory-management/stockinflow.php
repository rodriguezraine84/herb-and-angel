<!DOCTYPE html>
<?php 
include('../../connection.php');
session_start();

if(isset($_SESSION["code"]))
{
    $code = $_SESSION["code"];
}else{
    echo"Record not Found!";
    exit;
}

$name = "SELECT firstname, usertype FROM users WHERE code = $code";
$name_query = mysqli_query($conn, $name);

if(mysqli_num_rows($name_query) > 0){
    $row = mysqli_fetch_assoc($name_query);

    $fullname = $row["firstname"];
    $usertype = $row["usertype"];
}
date_default_timezone_set('Asia/Manila');

$product_query = "SELECT DISTINCT
                    i.date, 
                    p.productname,
                    i.supplier_price,
                    i.units_received,
                    i.totalvalue,
                    i.location
                    FROM
                    inflow_admin i
                    JOIN
                    products p ON i.barcode = p.barcode
                    ORDER BY i.date DESC";
$product_result = mysqli_query($conn, $product_query);

$product_query_branch = "SELECT DISTINCT
                            ib.date, 
                            p.productname,
                            ib.supplier_price,
                            ib.units_received,
                            ib.totalvalue,
                            br.branch_address AS location
                        FROM
                            inflow_branch ib
                            JOIN branch_record br ON ib.code = br.code
                            JOIN products p ON ib.barcode = p.barcode
                            ORDER BY ib.date DESC";
$product_result_branch = mysqli_query($conn, $product_query_branch);


$productname = "";

if (isset($_POST['barcode'])) {
    $barcode = mysqli_real_escape_string($conn, $_POST['barcode']);

    $product = "SELECT productname FROM products WHERE barcode= $barcode";
    $product_query = mysqli_query($conn, $product);

    
    if(mysqli_num_rows($product_query) > 0){
        $rows = mysqli_fetch_assoc($product_query);

        $productname = $rows["productname"];
    }
}

if(isset($_POST['addstocks']))
{
    $supplier = $_POST['supplierprice'];
    $units = $_POST['units'];
    $total_value = $supplier * $units;
    $location_admin = $_POST['location_admin'];
    // Using NOW() to insert the current date and time
    $inflow = "INSERT INTO inflow_admin (`date`, `barcode`, `supplier_price`, `units_received`, `totalvalue`, `code`, `location`) VALUES (NOW(), '$barcode', '$supplier', '$units', '$total_value', '$code', '$location_admin')";
    $inflow_query = mysqli_query($conn, $inflow);
    
}

if(isset($_POST['addstocksbranch'])){
    $barcode_branch = $_POST['barcodebranch'];
    $supplier_branch = $_POST['supplierpricebranch'];
    $units_branch = $_POST['unitsbranch'];
    $total_value_branch = $supplier_branch * $units_branch;
    $user_branch_code = $_POST['branch'];

    // Using NOW() to insert the current date and time
    $inflow_branch = "INSERT INTO inflow_branch (`date`, `barcode`, `supplier_price`, `units_received`, `totalvalue`, `code`) VALUES (NOW(), '$barcode_branch', '$supplier_branch', '$units_branch', '$total_value_branch', '$user_branch_code')";
    $inflow_branch_query = mysqli_query($conn, $inflow_branch);

}


?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern,  html5, responsive">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/animate.css">
    <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../../assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <script src="https://cdn.rawgit.com/serratus/quaggaJS/0.12.1/dist/quagga.min.js"></script>
    <title>Herb and Angel | Stock Inflow</title>
</head>
<style>
        .page-title {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .page-title h4 {
            margin: 0;
        }

        .btn-container {
            display: flex;
            gap: 10px; /* Adjust the gap between the button and the heading */
        }
    </style>
<body>
    <div id="global-loader">
        <div class="whirly-loader">

        </div>
    </div>

    <!--Main Content-->
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left active">
                <a href="./super-admin-dashboard.php" class="logo">
                    <img src="../../assets/img/logo (1).png" alt="">
                </a>

                <a href="super-admin-dashboard.php" class="logo-small">
                    <img src="../../assets/img/logo-small.png" alt="">
                </a>

                <a id="toggle_btn" href="javascript:void(0);"></a>
            </div>

            <a id="mobile_btn" class="mobile_btn" href="#sidebar">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>

            <!--Underline Nav-->
            <ul class="nav user-menu">

                <!--Nav Items Dropdown-->
                <li class="nav-item dropdown has-arrow main-drop">
                    <a href="javascript:void(0);" class="dropdown-toggle nav-link userset" data-bs-toggle="dropdown">
                        <span class="user-img"> <img src="../../assets/img/icons/users1.svg" alt="">
                        <span class="status online"></span></span>
                    </a>
                    <div class="dropdown-menu menu-drop-user">
                        <div class="profilename">
                            <div class="profileset">
                                <span class="user-img"><img src="../../assets/img/icons/users1.svg " alt="">
                                <span class="status online"></span></span>
                                <div class="profilesets">
                                <h6><?php echo $fullname?></h6>
                                <h5><?php echo $usertype?></h5>
                                </div>
                            </div>
                            <a class="dropdown-item logout pb-0" href="../../index.php"><img src="../../assets/img/icons/log-out.svg" class="me-2" alt="img">Logout</a>
                        </div>
                    </div>
                </li>
            </ul>

            <div class="dropdown mobile-user-menu">
                <a href="javascript:void(0);" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="../../index.php">Logout</a>
                </div>
            </div>
        </div>

        <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>
                                <li class="active">
                                    <a href="../super-admin-dashboard.php"><img src="../../assets/img/icons/dashboard.svg" alt="img"><span> Dashboard</span> </a>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-store-alt.svg" alt="img"><span> Dealer Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../dealer-management/records.php">Dealer Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-package.svg" alt="img"><span> Supplier Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../supplier-management/records.php">Supplier Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-git-branch.svg" alt="img"><span> Branch Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../branch-management/records.php">Branch Records</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-cog.svg" alt="img"><span> Services Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../services-management/services.php">Services</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../..//assets/img/icons/sales1.svg" alt="img"><span> Product Management</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../product-management/product-list.php">Add Product</a></li>
                                        <li><a href="../product-management/barcode.php">Barcode Printing</a></li>
                                        <li><a href="../product-management/barcode.php">Product Inventory</a></li>
                                    </ul>
                                </li>

                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-box.svg" alt="img"><span> Inventory Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../inventory-management/stockinflow.php">Stock Inflow</a></li>
                                                <li><a href="../inventory-management/stockoutflow.php">Stock Outflow</a></li>
                                                <li><a href="../inventory-management/inventoryanalyzer.php">Inventory Analyzer</a></li>
                                                <li><a href="../inventory-management/productcost.php">Product Cost</a></li>
                                            </ul>
                                </li>
                                
                                <li class="submenu">
                                        <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-user.svg" alt="img"><span> User Management</span> <span class="menu-arrow"></span></a>
                                            <ul>
                                                <li><a href="../user-management/accounts.php">Accounts</a></li>
                                            </ul>
                                </li>   

                                <li class="submenu">
                                    <a href="javascript:void(0);"><img src="../../assets/img/icons/bx-line-chart.svg" alt="img"><span> Report Generation</span> <span class="menu-arrow"></span></a>
                                    <ul>
                                        <li><a href="../report-generation/profit.php">Profit</a></li>
                                        <li><a href="../report-generation/mechanic.php">Mechanic</a></li>
                                
                                    </ul>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>

        <!--Under Main Content-->
        <div class="page-wrapper">
            <div class="content">
                <div class="page-title">
                    <h4>Stock Inflow</h4>
                    <div class="btn-container">
                        <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#addstocks" onclick="openPopup()">Add Stocks Admin</button>
                        <button type="button" class="btn btn-submit me-2" data-bs-toggle="modal" data-bs-target="#addstocksbranch" onclick="openPopupBranch()">Add Stocks Branch</button>
                    </div>
                </div>
                
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Supplier Price</th>
                            <th scope="col">Units Received</th>
                            <th scope="col">Total Value</th>
                            <th scope="col">Location</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        // Loop through the product results and display them in the table
                        while ($product_row = mysqli_fetch_assoc($product_result)) {
                            echo '<tr>';
                            echo '<td>' . $product_row['date'] . '</td>';
                            echo '<td>' . $product_row['productname'] . '</td>';
                            echo '<td>' . $product_row['supplier_price'] . '</td>';
                            echo '<td>' . $product_row['units_received'] . '</td>';
                            echo '<td>' . $product_row['totalvalue'] . '</td>';
                            echo '<td>' . $product_row['location'] . '</td>';
                            echo '</tr>';
                        }
                    ?>
                    
                    </tbody>
                    <tbody>
                    <?php
                        // Loop through the product results and display them in the table
                        while ($product_row_branch = mysqli_fetch_assoc($product_result_branch)) {
                            echo '<tr>';
                            echo '<td>' . $product_row_branch['date'] . '</td>';
                            echo '<td>' . $product_row_branch['productname'] . '</td>';
                            echo '<td>' . $product_row_branch['supplier_price'] . '</td>';
                            echo '<td>' . $product_row_branch['units_received'] . '</td>';
                            echo '<td>' . $product_row_branch['totalvalue'] . '</td>';
                            echo '<td>' . $product_row_branch['location'] . '</td>';
                            echo '</tr>';
                        }
                    ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
    
    <div class="popup" id="addstocks-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="addstocks" tabindex="-1" role="dialog" aria-labelledby="addstocksModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addstocksModalLabel">Add Product</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Barcode</label>
                                                    <input type="text" class="form-control" name="barcode" id="barcode" value="<?php echo @$_POST['barcode']?>" onblur="fetchProductName()" placeholder="Enter Barcode" autofocus>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Product Name</label>
                                                    <input type="text" class="form-control" name="productname" id="productname" value="<?php echo $productname ?>" placeholder="Enter Product Name" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Supplier Price</label>
                                                    <input type="text" class="form-control" name="supplierprice" id="supplierprice" placeholder="Enter Supplier Price" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Units Received</label>
                                                    <input type="text" class="form-control" name="units" id="units" placeholder="Enter Units Received" required>
                                                </div>
                                            </div>
                                            <!--eto inadd ko-->
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Admin Location</label>
                                                    <input type="text" class="form-control" name="location_admin" id="location_admin" value="Admin" placeholder="Admin" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="addstocks" class="btn btn-submit me-2">Add Stocks</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup" id="addstocksbranch-popup"> 
        <div class="popupcontent">
            <div class="modal fade" id="addstocksbranch" tabindex="-1" role="dialog" aria-labelledby="addstocksModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addstocksModalLabel">Add Product Branch</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Barcode</label>
                                                    <input type="text" class="form-control" name="barcodebranch" id="barcodebranch" value="<?php echo @$_POST['barcode']?>" onblur="fetchProductNameBranch()" placeholder="Enter Barcode" autofocus>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Product Name</label>
                                                    <input type="text" class="form-control" name="productnamebranch" id="productnamebranch" value="<?php echo $productname ?>" placeholder="Enter Product Name" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Supplier Price</label>
                                                    <input type="text" class="form-control" name="supplierpricebranch" id="supplierpricebranch" placeholder="Enter Supplier Price" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Units Received</label>
                                                    <input type="text" class="form-control" name="unitsbranch" id="unitsbranch" placeholder="Enter Units Received" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <label>Branch</label>
                                                    <select id="branch" name="branch" class="form-control" required>
                                                    <option value="" selected>Select a Branch</option>
                                                    <?php
                                                    $users_branch_query = mysqli_query($conn, "SELECT * FROM users_branch");

                                                    if(mysqli_num_rows($users_branch_query) > 0) {
                                                        while($user_row = mysqli_fetch_assoc($users_branch_query)) {
                                                            // Fetch the branch information for each user
                                                            $user_branch_code = $user_row["branch_code"];
                                                            $branch_info_query = mysqli_query($conn, "SELECT * FROM branch_record WHERE code = '$user_branch_code'");
                                                            
                                                            if(mysqli_num_rows($branch_info_query) > 0) {
                                                                $branch_info = mysqli_fetch_assoc($branch_info_query);
                                                                $branchName = $branch_info["branch_address"];
                                                                $branchCode = $branch_info["code"];

                                                                echo "<option value='$user_branch_code'>$branchName</option>";
                                                            } else {
                                                                echo "<option value=''>No branch found for user!</option>";
                                                            }
                                                        }
                                                    } else {
                                                        echo "<option value=''>No users found!</option>";
                                                    }
                                                    ?>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <button type="submit" name="addstocksbranch" class="btn btn-submit me-2">Add Stocks for Branch</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

<script src="../../assets/js/jquery-3.6.0.min.js"></script>
<script src="../../assets/js/feather.min.js"></script>
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap4.min.js"></script>
<script src="../../assets/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="../../assets/plugins/apexchart/chart-data.js"></script>
<script src="../../assets/js/script.js"></script>

<script>
    function openPopup() {
        document.getElementById("addstocks-popup").style.display = "block";
    }

    function openPopupBranch(){
        document.getElementById("addstocksbranch-popup").style.display = "block";
    }
</script>

<script>
    function fetchProductName() {
        var barcode = document.getElementById('barcode').value;
        if (barcode.trim() !== '') {
            $.ajax({
                type: 'POST',
                url: 'fetch_product.php',
                data: { barcode: barcode },
                success: function(response) {
                    document.getElementById('productname').value = response;
                },
                error: function(error) {
                    alert('Error fetching product name from the database.');
                    console.error(error);
                }
            });
        }
    }

    function fetchProductNameBranch() {
        var barcode = document.getElementById('barcodebranch').value;
        if (barcode.trim() !== '') {
            $.ajax({
                type: 'POST',
                url: 'fetch_product.php',
                data: { barcode: barcode },
                success: function(response) {
                    document.getElementById('productnamebranch').value = response;
                },
                error: function(error) {
                    alert('Error fetching product name from the database.');
                    console.error(error);
                }
            });
        }
    }
</script>
    
</body>
</html>