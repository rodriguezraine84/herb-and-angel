<?php
session_start();
include('../../connection.php');

// Fetch product name based on the scanned barcode
$barcode = $_POST['barcode'];
$sql = "SELECT products.productname FROM products 
        JOIN inflow_admin ON products.barcode = inflow_admin.barcode
        WHERE products.barcode = '$barcode'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // Return the product name
    $row = $result->fetch_assoc();
    echo $row['productname'];
} else {
    echo "Product not found";
}

// Close the connection
$conn->close();
?>
